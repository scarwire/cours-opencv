#!/usr/bin/env python3

import numpy as np
import cv2
import thresh

# chargement de l'image
image = cv2.imread("../images/IMGP2297-Pano.jpg")

# preprocessing
image = thresh.preprocess(image, kernel = 3)
cv2.imshow("Preprocessed", image)
cv2.waitKey(0)

# filtre de Canny
canny = cv2.Canny(image, 30, 150) # image, seuil1, seuil2
cv2.imshow("Canny", canny)
cv2.waitKey(0)
