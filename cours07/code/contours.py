#!/usr/bin/env python3

import numpy as np
import cv2
import thresh

# chargement de l'image
image = cv2.imread("../images/coins.png")
image = thresh.resize(image)

# preprocessing
preprocessed = thresh.preprocess(image)
cv2.imshow("Preprocessed", preprocessed)
cv2.waitKey(0)

# filtre de Canny
edged = cv2.Canny(preprocessed, 30, 150) # image, seuil1, seuil2
cv2.imshow("Canny", edged)
cv2.waitKey(0)

edged2 = edged.copy()
(cnts, _) = cv2.findContours(edged2, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
print("I count {} elements in this image".format(len(cnts)))

contours = cv2.drawContours(image, cnts, -1, (0, 0, 255), 2)
cv2.imshow("Contours", contours)
cv2.waitKey(0)

# extraire les éléments
for (i, c) in enumerate(cnts):
    (x, y, w, h) = cv2.boundingRect(c) # calcul du cadre (x début, y début, largeur, hauteur rectangle)

    print("Object #{}".format(i + 1))
    object = image[y:y + h, x:x+ w] # une fois le contour trouvé, extraction à l'aide d'un tableau
    cv2.imshow("Object", object)

    mask = np.zeros(image.shape[:2], dtype = "uint8")
    ((centerX, centerY), radius) = cv2.minEnclosingCircle(c) #contours circulaires
    cv2.circle(mask, (int(centerX), int(centerY)), int(radius), 255, -1)
    mask = mask[y:y + h, x:x + w]
cv2.imshow("Masked coin", cv2.bitwise_and(object, object, mask = mask))
cv2.waitKey(0)
