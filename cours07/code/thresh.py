import cv2
import numpy as np
import mahotas

def resize(image):
    '''
    Resize to 1000 pixels max on largest edge
    '''
    ratio = image.shape[1] / image.shape[0]
    if image.shape[1] > image.shape[0] and image.shape[1] > 1000:
        image = cv2.resize(image, (1000, int(1000 / ratio)), interpolation = cv2.INTER_AREA)
    elif image.shape[1] <= image.shape[0] and image.shape[0] > 1000:
        image = cv2.resize(image, (int(1000 * ratio), 1000), interpolation = cv2.INTER_AREA)
    return image

def preprocess(image, kernel: int = 5):
    '''
    Image preprocessing steps: resize, convert to grayscale, blur
    '''
    image = resize(image)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(image, (kernel, kernel), 0)
    return blurred

def simple(image, threshold: int, show = False):
    '''
    Simple threshold
    '''
    (T, thresh) = cv2.threshold(image, threshold, 255, cv2.THRESH_BINARY)
    if show is True:
        cv2.imshow("Simple threshold", thresh)
        cv2.waitKey(0)
    return thresh

def mean(image, blocksize: int = 11, C: int = 4, show = False):
    '''
    Mean threshold
    '''
    thresh = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, blocksize, C)
    if show is True:
        cv2.imshow("Mean threshold", thresh)
        cv2.waitKey(0)
    return thresh

def gaussian(image, blocksize: int = 15, C: int = 3, show = False):
    '''
    Gaussian threshold
    '''
    thresh = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, blocksize, C)
    if show is True:
        cv2.imshow("Gaussian threshold", thresh)
        cv2.waitKey(0)
    return thresh

def otsu(image, show = False):
    T = mahotas.thresholding.otsu(image)
    print("Otsu's thrshold: {}".format(T))
    thresh = image.copy()
    thresh[thresh > T] = 255
    thresh[thresh < 255] = 0
    if show is True:
        cv2.imshow("Otsu threshold", thresh)
        cv2.waitKey(0)
    return thresh

def rc(image, show = False):
    T = mahotas.thresholding.rc(blurred)
    print("Riddler-Calvard threshold: {}".format(T))
    thresh = image.copy()
    thresh[thresh > T] = 255
    thresh[thresh < 255] = 0
    if show is True:
        cv2.imshow("Riddler-Calvard", thresh)
        cv2.waitKey(0)
    return thresh
