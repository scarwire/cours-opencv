#!/usr/bin/env python3

import numpy as np
import cv2

# chargement de l'image
image = cv2.imread("../images/IMGP2297-Pano.jpg")

# redimensionnement de grandes images, max 1000 pixels côté long
ratio = image.shape[1] / image.shape[0]
if image.shape[1] > image.shape[0] and image.shape[1] > 1000:
    image = cv2.resize(image, (1000, int(1000 / ratio)), interpolation = cv2.INTER_AREA)
elif image.shape[1] <= image.shape[0] and image.shape[0] > 1000:
    image = cv2.resize(image, (int(1000 * ratio), 1000), interpolation = cv2.INTER_AREA)
cv2.imshow("Image", image)
cv2.waitKey(0)

# conversion en échelle de gris
image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# filtre sobel
# calcul du gradient sur chaque axe
sobelX = cv2.Sobel(image, cv2.CV_64F, 1, 0)
sobelY = cv2.Sobel(image, cv2.CV_64F, 0, 1)
# reconversion de float en int, matrice 8 bits
sobelX = np.uint8(np.absolute(sobelX))
sobelY = np.uint8(np.absolute(sobelY))

sobelCombined = cv2.bitwise_or(sobelX, sobelY)

cv2.imshow("Sobel X", sobelX)
cv2.imshow("Sobel Y", sobelY)
cv2.imshow("Sobel combined", sobelCombined)
cv2.waitKey(0)
