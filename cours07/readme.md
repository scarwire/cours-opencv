# D'autres méthodes de seuillage

## Otsu et Riddler-Calvard

- Autre méthode pour déterminer T
- La méthode d'Otsu suppose qu'il y a deux pics dans l'histogramme en niveaux de gris de l'image. Il tente ensuite de trouer une valeur optimale pour séparer ces deux.
- Ici utilisation de la méthode `mahotas` de python

## Otsu

```python
import numpy as np
import mahotas
import cv2

image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
blurred  = cv2.GaussianBlur(image, (5,5), 0)
cv2.imshow("Image", image)
cv2.waitKey(0)

T = mahotas.thresholding.otsu(blurred)
print("Otsu's thrshold: {}".format(T))

thresh = image.copy()
thresh[thresh > T] = 255
thresh[thresh < 255] = 0
cv2.imshow("Otsu", thresh)
cv2.waitKey(0)
```

Pour inverser les couleurs :

`thresh = cv2.bitwise_not(thresh)` ou inverser > et <


## Riddler-Calvard

- Utilisation de la méthode `rc`
- Code identique à celui de la méthode Otsu mais avec `T = mahotas.thresholding.rc(blurred)`

# Exercice : création de fonctions de seuil

Pour simplifier le processus, il s'agit maintenant de créer nos propres fonctions. Voir le fichier [`thresh.py`](code/thresh.py).

# Détection des gradients et des contours

- Formellement, la détection des contours incorpore des méthodes mathématiques pour trouver des points dans une image où la luminosité de l'intensité des pixels change nettement.
- La première chose que nous allons faire est de trouver le "gradient" de l'image en neveaux de gris, ce qui nous permettra de trouver des régions en bordure dans les directions x et y.
- Nous appliquerons ensuite la détection des bords de Canny, un procédé multi-étape e réduction du bruit (flou), la recherche du gradient de l'image (en utilisant le noyau Sobel à la fois dans les directions horizontale et verticale), la suppression non-maximale et le seuillage de l'hystérésis.

## Filtre laplacien

- Mettre l'image en échelle de gris : `image =cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)`

```python
lap = cv2.Laplacian(image, cv2.CV_64F)
lap = np.uint8(np.absolute(lap))
cv2.imshow("Laplacian", lap)
cv2.waitKey(0)
```

## Filtre de Sobel

```python
sobelX = cv2.Sobel(image, cv2.CV_64F, 1, 0)
sobelY = cv2.Sobel(image, cv2.CV_64F, 0, 1)
sobelX = np.uint8(np.absolute(sobelX))
sobelY = np.uint8(np.absolute(sobelY))
sobelCombined = cv2.bitwise_or(sobelX, sobelY)
cv2.imshow("Sobel X", sobelX)
cv2.imshow("Sobel Y", sobelY)
cv2.imshow("Sobel combined", sobelCombined)
cv2.waitKey(0)
```

## Filtre de Canny

Plusieurs étapes :

- Flouter l'image pour éliminer le bruit
- calculer les images de gradient de Sobel dans les directions X et Y
- supprimer les bords
- effectuer un seuil d'hystérésis qui détermine si un pixel est "en forme de bord" ou non

Paramètres de `cv2.Canny(image, T1, T2)` :

- pixel < T1 : considéré comme n'étant pas un contour
- pixel > T2 = contour
- T1 < pixel < T2 : évaluer selon l'intensité

Sur une image déjà prétraitée 

```python
canny = cv2.Canny(image, 30, 150) # image, seuil1, seuil2
cv2.imshow("Canny", canny)
cv2.waitKey(0)
```

# Contour

- Dans cette partie nous allons dénombrer le nombre d'éléments dans une image.
- OpenCV fournit des méthodes pour trouver des courbes. Un contour peut être considéré comme une courbe continue.

## `findContours`

- Charger une image, la transformer en niveaux de gris, appliquer un flou gaussien et un filtre de Canny
- `cv2.findContours` renvoie un tuple triple :
  1) notre image après détection des contours (qui a été modifiée et en partie détruite)
  2) les contours eux-mêmes
  3) la hiérarchie des contours
- Trois arguments :
  1) l'image (une copie de l'image)
  2) une méthode pour récupérer les contours extérieurs, `cv2.RETR_EXTERNAL`
  3) la méthode pour approximer les contours `CHAIN_APPROX_SIMPLE` compresse les contours horizontalement verticalement et diagonalement. Possibilité d'utiliser `cv2.CHAIN_APPROX_NONE` mais coûteux en temps et en mémoire

```python
(cnts, _) = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
print("I count {} coins in this image".format(len(cnts)))
```

## Afficher les contours

```python
cv2.drawContours(edged2, cnts, 0, (0, 255, 0), 2)
cv2.drawContours(edged2, cnts, 1, (0, 255, 0), 2)
cv2.drawContours(edged2, cnts, 2, (0, 255, 0), 2)
```

`drawContours` : image, la liste des contours, l'index des contours (-1 : tous les contours), la couleur du trait, l'épaisseur du trait

## Extraire les éléments

```python
for (i, c) in enumerate(cnts):
    (x, y, w, h) = cv2.boundingRect(c) # calcul du cadre (x début, y début, largeur, hauteur rectangle)

    print("Object #{}".format(i + 1))
    object = image[y:y + h, x:x+ w] # une fois le contour trouvé, extraction à l'aide d'un tableau
    cv2.imshow("Object", object)

    mask = np.zeros(image.shape[:2], dtype = "uint8")
    ((centerX, centerY), radius) = cv2.minEnclosingCircle(c) #contours circulaires
    cv2.circle(mask, (int(centerX), int(centerY)), int(radius), 255, -1)
    mask = mask[y:y + h, x:x + w]
cv2.imshow("Masked coin", cv2.bitwise_and(object, object, mask = mask))
cv2.waitKey(0)
```
