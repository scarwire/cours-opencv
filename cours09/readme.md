# Note : options de `cv2.imread`

Arguments :

- 1 : `cv2.IMREAD_COLOR` : valeur par défaut. Charge une image en couleurs sans la transparence.
- 0 : `cv2.IMREAD_GRAYSCALE` : charge une image en niveaux de gris
- -1 : `cv2.IMREAD_UNCHANGED` : charge une image en couleurs en incluant le canal alpha (transparence)

donc : 

`cv2.imread("chemin/image"[, 1/0/-1])`

# Application du _machine learning_

## Pipeline de la classification avec _deep learning_

- Au lieu d'essayer de construire un sysètme basé sur des règles pour décrire à quoi ressemble chaque catégorie, nous pouvons plutôt adopteer une approche axée sur les données en fournissant des exemples de ce à quoi ressemble chaque catégorie et ensuite ensigner à notre algorithme à reconnaître la différence entre les catégories en utilisant ces exemples.
- Ces exemples notre _training dataset_ d'images étiquetées, où chaque data point du dataset se compose :
  - d'une image
  - des labels ou catégories de chaque image

## Construction d'un classificateur

- Pour ce faire, quatre étapes de la construction d'un classificateur d'images basé sur le _deep learning_ sont nécessaires :
  1. constituer le dataset ;
  2. subdiviser le dataset ;
  3. entraîner le réseau ;
  4. évaluer les résultats.

## Constitution du dataset

- Nous avons besoin des images elles-mêmes ainsi que les étiquettes associées à chaque image
- Ces étiquettes doivent appartenir à un ensemble **fini** de catégories. Par exemple : `categories = ['chien', 'chat', 'souris', 'banane']`
- Le nombre d'images pour chaque catégorie devrait être approximativement uniforme, soit le même nombre d'exemples par catégorie.
- Si nous avons deux fois plus d'images de chats que de chiens et cinq fois plus d'images de panda que d'images de chat, alors notre classificateur deviendra naturellement biaisé avec un _overfitting_ de la catégorie la plus présente.
- Le déséquilibre des classes est un problème courant dans le deep learning. Il existe plusieurs façons d'y remédier Plusieurs méthodes existent, la meilleur est d'éviter le déséquilibre entre les classes.

## Subdiviser le dataset

- Une fois le dataset initial créé on va le diviser en deux parties :
  - un training set
  - un testing set
- Le training set est utilisé par contre classificateur pour "apprendre" à quoi ressemble chaque catégorie en utilisant les données d'entrée, puis se corriger lui-même lorsque les prédictions sont fausses
- Quand le classificateur a été formé, nous pouvons évaluer les performances sur un testing set
- Il est extrêmement important que le training set et le testing set soient indépendants l'un de l'autre : si des images sont présents dans les deux, alors le classificateur aura un avantage sur les données de test car il aura déjà "appris" à reconnaître cette image.
- Penser à toujours garder les données du training set séparé du testing set.

### Divisions entre testing/training

- Le training set est toujours plus grand
- Rapports testing::training :
  - 33:67
  - 25:75
  - 10:90

### Hyperparamètres

- Les réseaux neuronaux ont un certain nombre de boutons et de leviers : par exemple, taille du kernel, vitesse d'apprentissage, dégradation des poids, régularisation etc.
- En pratique, nous devons tester un certain nombre de ces hyperparamètres qui ..

### Set de validation

- Cet ensemble de données provient des données d'entraînement et est utilisé comme "fausses données d'essai" pour que nous puissions régler nos hyperparamètres.
- Ce n'est qu'après avoir déterminé les valeurs des hyperparamètres à l'aide de l'ensemble de validation que nous avons déterminé nous passons à la collecte des résultats finaux de l'exactitude des données d'essai.
- Normalement, nous allouons environ 10 à 20% des données de formation à la validation.
- Séparer le data set initial est très simple et peut être fait avec une seule ligne de code grâce à la bibliothèque `scikit-learn`.

## Entraîner le réseau

- Grâce à notre training set, nous pouvons entraîner notre réseau.
- L'objectif ici est que notre réseau apprenne à reconnaître chacune des catégories de nos données étiquetées.
- Lorsque le modèle commet une erreur, il en tire les leçons et s'améliore.

## L'évaluation

- Pour chacune des images du testing set, nous demandons au réseau de prédire ce qu'il pense être l'étiquette de l'image.
- Ces prédictions du modèle sont comparées aux étiquettes des images.
- À partir de là, nous pouvons calculer le nombre de prédictions que notre classificateur a obtenues correctement et calcule des rapports globaux tels que la précision, de rappel (recall) et de mesure f, qui servent à quantifier le rendement de l'ensemble de notre réseau.

## Généralisation

- Après avoir entraîné et évalué vos résultats, vous pouvez obtenir un taux de précision très élevé. Vous allez appliquer votre réseau sur des images nouvelles et il fonctionnera très mal.
- Ce problème s'appelle la généralisation : la capacité pour un réseau de généraliser et de prédire correctement l'étiquette de classe d'une image qui n'existe pas dans le training ou testing set.
- La capacité d'un réseau à généraliser est littéralement l'aspect le plus important d'un réseau profond. L'apprentissage de la recherche -- si nous pouvons former des réseaux qui peuvent généraliser à des ensembles de données externes cela permettra de le réutiliser dans plusieurs domaines.
- En cas d'overfitting : est-ce que le training set reflète tous les facteurs de variations de vos images à classifier ? Si c'est le cas, d'autres techniques existent, qu'on verra plus loin.

## Les datasets à disposition

- MNIST ; animals : dogs, cats and pandas ; CIFAR-10 ; SMILES
- Kaggle : dogs vs. cats ; flowers-17 ; CALTECH-101 ;
- Tiny ImageNet 200 : Adience ; ImageNet ; Kaggle : facial expression recognition challenge ; indoor CVPR ; Stanford cars ; LISA traffic signs ; front/rear view vehicles

### MNIST

- Classifier les chiffres écrits à la main de 0 à 9
- 60000 images de test et 1000 pour validation en échelle de gris de 28 \* 28 pixels.

### Animals : dogs, cats & pandas

- ~1000~ beaucoup d'images pour chaque animal

### CIFAR-10

- 60000 images en 32\*32\*3 (rgb). Résultat : un _feature vector_ de dimensionnalité de 3072
- Le défi vient de la grande variété de commen un objet apparaît
- nombreuses catégories

### SMILE

- 13165 images en niveaux de gris où des personnes sourient ou non. Images de 64 \* 64.
- À noter que les images reeprésentent uniquement des figures et non des personnes en entier.

### Kaggle : dogs vs. cats

- 25000 images de résolutions différentes

### Flowers 17

- Peut être considéré comme un ensemble de données difficile en raison des changements spectaculaires d'échelle, d'angles de vue, de la multiplicité du bruit de fond, des conditions d'éclairage et des variations intra-classe.
- Avec seulement 80 images par classe, il devient difficile pour les modèles de deep learning d'apprendre une représentation pour chaque classe sans overfitting.

### CALTECH-101

- 8677 images comprenant 101 catégories couvrant un large éventail d'objets (éléphants, bicyclettes, ballons de foot, cerveaux humains...)
- Présence de forts déséquilibres de classe (ce qui signifie qu'il y a plus d'images d'exemple pour certaines catégories que pour d'autres), ce qui rend l'étude à partir d'un déséquilibre de classe intéressante.

## Pour aller plus loin : `mxnet`

- Une bibliothèque de deep learning spécialisée dans l'apprentissage distribué et multi-machines.
- La possibilité de paralléliser l’entraînement du réseau entre plusieurs GPU/périphériques pour des dataset importants (comme ImageNet) est essentielle.
- Ici, on ne l'utilisera pas.

## Premier classificateur : k-Nearest Neighbors (k-NN)

- (k-NN) classifier : utilisation du machine learning pour classer une image.
- Cet algorithme est si simple qu'on ne peut pas l'appeler "apprendre".
- Intéressant pour voir comment un réseau neuronal apprend des données.
- Application d'un algorithme k-NN pour reconnaître diverses espèces d'animaux en images.

### Architecture du projet

```
cours9
|--- knn.py
|--- pyimagesearch
||--- __init__.py
||--- datasets
|||--- __init__.py
|||--- simpledatasetloader.py
||--- preprocessing
|||--- __init__.py
|||--- simplepreprocessor.py
|--- images
```
### `SimpleDatasetLoader`

- Les sous-modules datasets vont démarrer notre implémentation d'une classe nommée `SimpleDatasetLoader`. Nous utiliserons cette classe pour charger de petits ensembles de données d'images à partir du disque (qui peuvent tenir dans la mémoire principale), en option prétraiter chaque image de l'ensemble de données en fonction d'un ensemble de fonctions, puis retourner le :
  1. images (c'est à dire l'intensité brute des pixels)
  2. étiquette de classe associée à chaque image

### `SimplePreprocessor`

- Nous avons ensuite le sous-modèle de prétraitement. Comme nous le verrons dans les chapitres suivants, il existe de nombreuses méthodes de prétraitement que nous pouvons appliquer à notre ensemble de données d'images pour améliorer la classification y compris la soustraction de la moyenne, l'échantillonnage de patchs aléatoires, ou simplement le redimensionnement de l'image à un format taille fixe.
- Dans notre cas, notre classe `SimplePreprocessor` se chargera de ce dernier -- charger une image à partir de et la redimensionner à une taille fixe, sans tenir compte du ratio d'image.

### Resize

- Les algorithmes d'apprentissage machine tels que k-NN, SVM et même les réseaux neuronaux convolutionnels exigent que toutes les images d'un ensemble de données aient une taille de vecteur de caractéristique fixe. Dans le cas des images, cela implique que nos images doivent être prétraitées et mises à l'échelle pour avoir des largeurs et des hauteurs identiques.

## k-NN

Pour que l'algorithme k-NN fonctionne, il part du principe que les images ayant un contenu visuel similaire sont proches les unes des autres dans un espace n-dimensionnel. Ici, nous pouvons voir trois catégories d'images, désignées respectivement par chiens, chats et pandas. Dans cet exemple, nous avons tracé la "moelleux" du pelage de l'animal sur l'axe des x et la "légèreté" du pelage sur l'axe du y. ...

## 2 hyperparamètres

- K : le nombre de voisins à prendre en compte
- La distance : Euclidienne ou Manhattan ?

## Notre Pipeline

- Le but de cette section est de former un classificateur k-NN sur l'intensité prute des pixels du dataset animaux et de l'utiliser pour classer des images d'animaux inconnus.

Étapes :

1. Recueillir notre dataset : 3000 images dont 1000 images par classe de chien, chat et panda respectivement. Chaque image est représentée dans l'espace couleur RVB. Nous prétraiterons chaque image en la redimensionnant à 32\*32 pixels. Prenant en considération les trois canaux RVB, les dimensions de l'image redimensionnée impliquent que chaque image dans le dataset est représenté par 32\*32\*3 = 3072 entiers.
2. Diviser l'ensemble de données : pour cet exemple simple, nous utiliserons deux divisions des données. l'un pour la formation et l'autre pour les tests. Nous ne tiendrons pas compte de l'ensemble de validation pour hyperparamètre.
3. Former le classificateur : notre classificateur k-NN sera formé sur l'intensité brute des pixels du training set.
4. Évaluer : une fois que notre classificateur k-NN est formé, nous pouvons évaluer la performance sur le testing set.

## Results

- The **precision** is the ration tp / (tp + fp) where tp is the number of true positives and fp the number of false positives. The precision is intuitively  the ability of the classifier not to label as positive a sample that is negative.
- The **recall** is the ratio of tp / (tp + fn) where fn is the number of false negaties. The recall is intuitively the ability of the classifier to find all the positive samples.
- The **F-beta score** can be interpreted as a weighted harmonic mean of the precision and recall, where an F-beta score reaches its best value at 1 and worst score at 0.
- The F-beta score weights recall more than precision by a factor of beta. beta == 1.0 means recall and precision are equally important.
- The **support** is the number of occurrences of each class in y_true.

# Parametric model

> A learning model that summarizes data with a set of parameters of fixed size (independent of the number of training examples) is called a parametric model. No matter...

- Parameterization is the process of defining the necessary parameters of a given model. In the task of machine learning, parameterization involves defining a problem in terms of four key components: data, a scoring function, a loss function, and weights and biases.

## data

- This component is our input data that we are going to learn from. this data includes both the data points (i.e. raw pixel intensities from images, extracted freatures, etc.) and their associated class labels. Typically we ndenote our data in terms of a multi-dimensional design matrix.
- Each row in the design matrix represents a data point while each column (which itself could be a multidimensionnal array) of the matrix corresponds to a different feature.
- For example, consider a dataset of 100 images in the RGB color space. Each image 32\*32 pixels. The design matrix owuld be X R100 (32\*32\*3)...

## scoring function

- The scoring function accepts our data as an input and maps the data to class labels. For instance, given our set of input images, the scoring function takes these data poiints, applies some function f (our scoring function) and then returns the predicted class labels, similar to the pseudocode below:

`INPUT IMAGES => F(INPUT_IMAGES) => OUTPUT_CLASS_LABELS`

## loss function

- A loss function qunatifies how well our predicted class labels agree with our ground-truth labels.
- The higher level of agreement between these two sets of labels, the lower our loss (and higher our classification accuracy, at least on the training set).

## weights and biases (paramètres)

- The weight matrix, typically denoted as W and the bias vector b are called the weights or parameters of our classifier that we'll actually be optimizing. Based on the output of our scoring function and loss function, we'll be adjusting the values of the weights and viases in order to increase accuracy.
- Depending on model type, there may exist more parameters, but these are the four building blocks of parameterized learning that you'll commonly encounter.
- Once we've defined these four key components, we can then apply optimization methods that allow us to find a set of parameters...
