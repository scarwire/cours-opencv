#!/usr/bin/env python3

import numpy as np
import cv2
from matplotlib import pyplot as plt

image = cv2.imread("../images/IMGP0113.jpg")

# redimensionnement de grandes images, max 1000 pixels côté long
ratio = image.shape[1] / image.shape[0]
if image.shape[1] > image.shape[0] and image.shape[1] > 1000:
    image = cv2.resize(image, (1000, int(1000 / ratio)), interpolation = cv2.INTER_AREA)
elif image.shape[1] <= image.shape[0] and image.shape[0] > 1000:
    image = cv2.resize(image, (int(1000 * ratio), 1000), interpolation = cv2.INTER_AREA)

def plot_histogram(image, title, mask = None):
    chans = cv2.split(image)
    colors = ("b", "g", "r")
    plt.figure()
    plt.title(title)
    plt.xlabel("Bins")
    plt.ylabel("Number of pixels")
    for (chan, color) in zip(chans, colors): # boucle pour tous les canaux de l'image
        hist = cv2.calcHist([chan], [0], mask, [256], [0, 256])
        plt.plot(hist, color = color)
        plt.xlim([0, 256])

cv2.imshow("Original", image)
plot_histogram(image, "Histogram for original image")

# masque rectangulaire
mask = np.zeros(image.shape[:2], dtype="uint8") #création d'une canevas de la même taille que notre image
(cX, cY) = (image.shape[1] // 4, image.shape[0] // 3) # définition du centre de l'image
cv2.rectangle(mask, (cX - 200, cY - 200), (cX + 200, cY + 200), 255, -1) # création d'un rectangle sur le masque de la taille souhaité. X et Y de début, X et Y de fin
cv2.waitKey(0)
masked = cv2.bitwise_and(image, image, mask = mask) # tous les pixels de l'image seront "vrais", en appliquant le masque, seuls les pixels du masque seront actifs
cv2.imshow("Mask applied to image", masked)
cv2.waitKey(0)

plot_histogram(image, "Histogram of mask", mask = mask)
plt.show()
