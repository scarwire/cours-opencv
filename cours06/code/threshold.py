#!/usr/bin/env python3

import numpy as np
import cv2
from matplotlib import pyplot as plt
import argparse

# chargement de l'image depuis la ligne de commande
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
ap.add_argument("-t", "--threshold", required=True, help="Threshold")
args = vars(ap.parse_args())
image = cv2.imread(args["image"])
threshold = int(args["threshold"])

# redimensionnement de grandes images, max 1000 pixels côté long
ratio = image.shape[1] / image.shape[0]
if image.shape[1] > image.shape[0] and image.shape[1] > 1000:
    image = cv2.resize(image, (1000, int(1000 / ratio)), interpolation = cv2.INTER_AREA)
elif image.shape[1] <= image.shape[0] and image.shape[0] > 1000:
    image = cv2.resize(image, (int(1000 * ratio), 1000), interpolation = cv2.INTER_AREA)

image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(image, (5, 5), 0)
cv2.imshow("Image", image)
cv2.waitKey(0)

(T, threshInv) = cv2.threshold(blurred, threshold, 255, cv2.THRESH_BINARY_INV) #image, seuil, valeur du pixel, méthode : les pixels > à T = 3e argument
cv2.imshow("Threshold binary inverse", threshInv)
cv2.waitKey(0)

# Inverser la binarisation
(T, thresh) = cv2.threshold(blurred, threshold, 255, cv2.THRESH_BINARY)
cv2.imshow("Threshold binary", thresh)
cv2.waitKey(0)

# Afficher les éléments avec un fond noir
cv2.imshow("Fond noir", cv2.bitwise_and(image, image, mask=threshInv))
cv2.waitKey(0)

# Seuil moyen
thresh = cv2.adaptiveThreshold(blurred, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 11, 4)
cv2.imshow("Mean threshold", thresh)
cv2.waitKey(0)

# Seuil gaussien
thresh = cv2.adaptiveThreshold(blurred, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 15, 3)
cv2.imshow("Gaussian threshold", thresh)
cv2.waitKey(0)
