# Égalisation d'histogramme

[Script](code/egal.py) qui applique l'égalisation à une image. L'effet est particulièrement visible sur [l'image `IMGP0113.jpg`](images/IMGP0113.jpg).

- l'égalisation d'histogramme améliore le contraste d'une image en "étirant" la distribution des pixels
- Considérez un histogramme avec un grand pic au centre. L'application de l'égalisation d'un histogramme étirera le pic vers le coin de l'image, améliorant ainsi le contraste global de l'image.
- L'égalisation des histogrammes est appliquée aux images en niveaux de gris. Cette méthode est utile lorsqu'une image contient des avant-plans et des arrière-plans foncés ou clairs à la fois
- Elle a tendance à produire des effets irréalistes sur les photographies. Cependant, elle est utile lorsqu'il s'agit d'améliorer le contraste des images médicales ou satellites.

```python
image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) # convertir l'image en niveaux de gris
eq = cv2.equalizeHist(image) # application de l'égalisation d'histogramme
cv2.imshow("Histogram equalization", np.hstack([image, eq]))
cv2.waitKey(0)
```

# Histogramme et masque

[Ce script](code/hist+masque.py) applique un masque à une partie précise de `IMGP0113.jpg`, définit une fonction pour créer un histogramme, puis applique cette fonction (1) à l'image originale et (2) à la zone sélectionnée seulement.

## Fonction histogramme avec `numpy` et `matplotlib`

```python
def plot_histogram(image, title, mask = None):
    chans = cv2.split(image)
    colors = ("b", "g", "r")
    plt.figure()
    plt.title(title)
    plt.xlabel("Bins")
    plt.ylabel("Number of pixels")
    for (chan, color) in zip(chans, colors): # boucle pour tous les canaux de l'image
        hist = cv2.calcHist([chan], [0], mask, [256], [0, 256])
        plt.plot(hist, color = color)
        plt.xlim([0, 256])

cv2.imshow("Original", image)
plot_histogram(image, "Histogram for original image")

plt.show()
```

# Lissage et floutage

[Le script produit dans cette partie du cours](code/blur.py) applique plusieurs méthodes de floutage à une image.

- C'est ce qui arrive quand un appareil photo prend une photo hors mise au point
- En pratique, cela signifie que chaque pixel de l'image est mélangé avec l'intensité des pixels qui l'entourent. Ce "mélange" de pixels dans un quartier devient notre pixel flou.
- Bien que cet effet soit généralement indésirable sur nos photos, il est en fait très utile lors de l'exécution de tâches de traitement d'images.
- En fait, de nombreuses fonctions de traitement d'image et de vision par ordinateur, telles que la binarisation (fonction seuil) et la détection des contours, s'exécutent mieux si l'image est d'abord lissée ou floutée

## Moyenne

- Définition d'une fenêtre glissante k x k en haut de notre image (k doit toujours être impair)
- Cette fenêtre va glisser de gauche à droite et de haut en bas. le pixel au centre de cette matrice (nous devons utiliser un nombre impair, sinon il n'y aurait pas de véritable "centre") est alors fixé à la moyenne de tous les autres pixels qui l'entourent.
- Nous appelons cette fenêtre coulissante un "noyau de convolution" ou simplement un "noyau" (kernel)
- Que se passe-t-il en augmentant la taille de notre fenêtre ? --> plus flou

## Flou moyen

- `cv2.blur(image, (taille kernel x, taille kernel y))`

```python
blurred = np.hstack([
cv2.blur(image, (3, 3)),
cv2.blur(image, (5, 5)),
cv2.blur(image, (7, 7))
])
cv2.imshow("Averaged", blurred)
cv2.waitKey(0)
```
## Flou gaussien

- Similaire au flou moyen, mais au lieu d'utiliser une moyenne simple, nous utilisons maintenant une moyenne pondérée, où les pixels de voisinage qui sont plus proches du pixel central contribuent davantage au poids de la moyenne.
- Le résultat final est que notre image est moins floue, mais plus "naturellement" floue, qu'en utilisant la méthode moyenne.
- On va utiliser la fonction `cv2.GaussianBlur(image, (taille du kernel x, taille kernel y), écart type x, écart type y)`

```python
blurred = np.hstack([
cv2.GaussianBlur(image, (3, 3), 0),
cv2.GaussianBlur(image, (5, 5), 0),
cv2.GaussianBlur(image, (7, 7), 0),
])
cv2.imshow("Gaussian", blurred)
cv2.waitKey(0)
```

## Flou médian

- Définition de la taille de notre noyau k. Comme dans la méthode du flou moyen, nous prenons en compte tous les pixels du voisinage de la taille k x k.
- Contrairement à la méthode du flou moyen, au lieu de remplacer le pixel central par la moyenne du voisinage, nous remplaçons plutôt le pixel central par le médian du voisinage.
- Le flou médian es plus efficace pour supprimer le bruit de type sel et poivre d'une image parce que chaque pixel central est toujours remplacé par une intensité de pixel qui existe dans l'image.
- Les méthodes d'établissement de la moyenne et les méthodes gaussiennes peuvent calculer des moyennes ou des moyennes pondérées pour le voisinage -- cette intensité moyenne des pixels peut ou non être présente dans le voisinage. Mais par définition, le pixel médian doit exister dans notre voisinage.
- Fonction : `cv2.medianBlur(image, diamètre des pixels voisins)`

```python
blurred = np.hstack([
cv2.medianBlur(image, 3),
cv2.medianBlur(image, 5),
cv2.medianBlur(image, 7)
])
cv2.imshow("Median", blurred)
cv2.waitKey(0)
```

## Flou bilatéral

- Afin de réduire le bruit tout en conservant les bords, nous pouvons utiliser le flou bilatéral grâce à deux distributions gaussiennes.
- La première fonction gaussienne ne considère que les voisins spatiaux, c'est-à-dire les pixels qui apparaissent proches les uns des autres dans l'espace des coordonnées (x, y) de l'image.
- La seconde gaussienne modélise l'intensité des pixels du voisinage, en s'assurant que seuls les pixels d'intensité similaire sont inclus dans le calcul réel du flou.
- Dans l'ensemble, cette méthode permet de préserver les bords d'une image tout en réduisant le bruit. Le principal inconvénient de cette méthode est qu'elle est considérablement plus lente que la moyenne, le gaussien et le flou médian.
- Fonction : `cv2.bilateralFilter(image, diamètre des pixels voisins, taille x du kernel, taille y du kernel)`

```python
blurred = np.hstack([
cv2.bilateralFilter(image, 5, 21, 21),
cv2.bilateralFilter(image, 7, 31, 31),
cv2.bilateralFilter(image, 19, 41, 41),
])
cv2.imshow("Bilateral", blurred)
cv2.waitKey(0)
```

# Seuillage/binarisation

[Le dernier script de la journée](code/threshold.py) applique plusieurs fonctions de seuil à une image. Il prend deux arguments à la ligne de commande : `-i fichier_image` pour choisir le fichier d'image et `-t seuil`, où `seuil` est une valeur entre 0 et 255 correspondant au niveau du seuil pour les fonctions de seuil simple. On voit l'effet du script particulièrement bien sur des images avec un fond uni à supprimer : par exemple [`IMGP3101.JPG`](images/IMGP3101.JPG) (essayer avec `-t 95`) ou [`coins.png`](images/coins.png) (essayer avec `-t 160`).

- En général, nous cherchons à convertir une image en niveaux de gris en une image binaire, où les pixels sont 0 ou 255. Un exemple simple de seuillage consisterait à sélectionner une valeur de pixel p, puis à mettre à zéro toutes les intensités de pixels inférieures à p, et toutes les valeurs de pixels supérieures à p à 255. De cette façon, nous pouvons créer une représentation binaire de l'image.
- Nous utilisons le seuillage pour faire la mise au point sur des objets ou des objets d'intérêt particulier dans une image.

## Seuillage simple

- L'application de méthodes simples de seuillage nécessite une intervention humaine.
- Nous devons spécifier une valeur seuil T.
- Toutes les intensités de pixels inférieures à T sont réglées à 0 et toutes les intensités supérieurs à T sont réglées à 255.
- On peut aussi appliquer l'inverse de cette binarisation en réglant tous les pixels en dessous de T à 255 et les intensités de pixels supérieurs à T à 0.

## Seuil adaptatif

- Fixer le seuil : contrainte de trouver le bon, nécessite beaucoup d'essais, n'est pas forcément le même pour toutes les parties de l'image
- Seuil adaptatif : prendre en compte les voisins d'un pixel et trouver un seuil différent selon les parties de l'image. Pas besoin de fixer manuellement.
- Utile si les pixels d'une image ont une grande variation
- Du coup, on va avoir un **seuil moyen** et un **seuil gaussien**

## Seuil moyen

```python
thresh = cv2.adaptiveThreshold(blurred, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 11, 4)
cv2.imshow("Mean threshold", thresh)
cv2.waitKey(0)
```

- 6 arguments de la fonction `cv2.adaptiveThreshold` :
  - l'image
  - la valeur du pixel
  - la méthode pour calculer le seuil selon les voisins et appliquer la valeur T
  - la méthode pour binariser
  - la taille du voisinage
  - la valeur soustraite à la moyenne pour affiner notre seuillage

## Seuil gaussien

- Utilisation d'une moyenne pondérée : gaussienne

```python
thresh = cv2.adaptiveThreshold(blurred, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 15, 3)
cv2.imshow("Gaussian threshold", thresh)
cv2.waitKey(0)
```
