#!/usr/bin/env python3

import cv2
import numpy as np
import imutils

lena = cv2.imread("images/lena.png")
marie = cv2.imread("images/marie.png")
images = [lena, marie]

for image in images:
    imutils.compareblurs(image)

# comparing the blur functions with the human eye, the best results come from:
# median blur with a 3-pixel kernel for Lena
# median blur with a 7-pixel kernal for Marie
# Let's try median blurs with an even larger kernels for Marie to see how they go:

blurred = np.hstack([
cv2.medianBlur(marie, 9),
cv2.medianBlur(marie, 11),
cv2.medianBlur(marie, 13),
cv2.medianBlur(marie, 15),
cv2.medianBlur(marie, 17),
cv2.medianBlur(marie, 19)
])
cv2.imshow("Median", blurred)
cv2.waitKey(0)

# an 11-pixel kernel seems to have a good compromise between a relatively clean image and acceptable amount of blurring/data loss

lena_restored = cv2.medianBlur(lena, 3)
marie_restored = cv2.medianBlur(marie, 11)

try:
    cv2.imwrite("result/lena_restored.png", lena_restored)
    cv2.imwrite("result/marie_restored.png", marie_restored)
except Exception:
    print("Erreur d'écriture de fichier")
