#!/usr/bin/env python3

import cv2
import numpy as np
import imutils

brain = cv2.imread("images/cerveau.png")
cv2.imshow("Brain", brain)
cv2.waitKey(0)

(_, G, _) = cv2.split(brain)

gauss = cv2.GaussianBlur(G, (5, 5), 0)
(_, thresh) = cv2.threshold(gauss, 7, 255, cv2.THRESH_BINARY_INV)
edged = cv2.Canny(thresh, 10, 150) # image, seuil1, seuil2
cv2.imshow("Canny", edged)

edged2 = edged.copy()
(cnts, _) = cv2.findContours(edged2, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
print(f"Régions bleues trouvées dans l'image : {len(cnts)}")
contours = cv2.drawContours(brain, cnts, -1, (127, 0, 255), 2)
cv2.imshow("Contours", contours)
cv2.waitKey(0)
