#!/usr/bin/env python3

import cv2
import numpy as np
import imutils

# ouvrir original
image = cv2.imread("images/billes.jpg")
cv2.imshow("Original", image)

# rotation 34° antihoraire, du coin supérieur droite
rotated = imutils.rotate(image, -34, (image.shape[1], 0))
cv2.imshow("Rotated", rotated)

# resize
resized = imutils.resize(image, (image.shape[1] // 3))
cv2.imshow("Resized", resized)

# translation 20 pixels en haut, 70 pixels à droite
translated = imutils.translate(image, 70, -20)
cv2.imshow("Translated", translated)

# recadrage autour de la bille jaune
yellow = image[314:391, 158:235]
cv2.imshow("Yellow marble", yellow)
cv2.waitKey(0)

# sauvegarde de l'image de la bille jaune
try:
    cv2.imwrite("result/bille_jaune.png", yellow)
except Exception:
    print("Échec d'écriture du fichier")
