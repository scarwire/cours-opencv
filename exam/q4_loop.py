#!/usr/bin/env python3

import cv2
import numpy as np
import imutils

brain = cv2.imread("images/cerveau.png")
cv2.imshow("Brain", brain)
cv2.waitKey(0)

height = brain.shape[0]
width = brain.shape[1]
blue_regions = brain.copy()

# loop through all the pixels, setting all non-blue pixels to black
x_position, y_position = 0, 0
while x_position < width:
    while y_position < height:
        (b, g, r) = blue_regions[y_position, x_position]
        if b < 50:
            blue_regions[y_position, x_position] = (0, 0, 0)
        else:
            if g > 50 or r > 50:
                blue_regions[y_position, x_position] = (0, 0, 0)
        y_position += 1
    y_position = 0
    x_position += 1

cv2.imshow("Blue regions", blue_regions)
cv2.waitKey(0)

# Now we have an image of only the blue regions.
# We can use the same method to detect objects that we used in question 3:
# preprocessing, Canny filter for contour detection, then count
# the objects and display the contours against the original im127age.

preprocessed = imutils.preprocess(blue_regions)
edged = cv2.Canny(preprocessed, 10, 70) # image, seuil1, seuil2
#cv2.imshow("Canny", edged)

edged2 = edged.copy()
(cnts, _) = cv2.findContours(edged2, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
print(f"Régions bleues trouvées dans l'image : {len(cnts)}")
contours = cv2.drawContours(brain, cnts, -1, (127, 0, 255), 2)
cv2.imshow("Contours", contours)
cv2.waitKey(0)
