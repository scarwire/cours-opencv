import numpy as np
import cv2
try:
    import mahotas
    mahotas_present = True
except ImportError:
    mahotas_present = False
    print("Mahotas library missing. Otsu and RC threshold functions not available.")

# Translation
def translate(image, x, y):
    M = np.float32([[1, 0, x], [0, 1, y]])
    shifted = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))
    return shifted

# Rotation
def rotate(image, angle, center = None, échelle = 1.0):
    (h, w) = image.shape[:2]
    if center is None:
        center = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D(center, angle, échelle)
    rotated = cv2.warpAffine(image, M, (w, h))
    return rotated

# Redimensionnement
def resize(image, width = None, height = None, inter = cv2.INTER_AREA):
    dim = None
    (h, w) = image.shape[:2]
    if width is None and height is None:
        return image
    if width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    else:
        r = width / float(w)
        dim = (width, int(h * r))
    resized = cv2.resize(image, dim, interpolation = inter)
    return resized

def max1000px(image):
    '''
    Resize to 1000 pixels max on largest edge
    '''
    ratio = image.shape[1] / image.shape[0]
    if image.shape[1] > image.shape[0] and image.shape[1] > 1000:
        image = cv2.resize(image, (1000, int(1000 / ratio)), interpolation = cv2.INTER_AREA)
    elif image.shape[1] <= image.shape[0] and image.shape[0] > 1000:
        image = cv2.resize(image, (int(1000 * ratio), 1000), interpolation = cv2.INTER_AREA)
    return image

def relresize(image_a, image_b, inter = cv2.INTER_AREA):
    '''
    Resizes two images so that one will fit inside the other. Returns a tuple of images.
    '''
    (height_a, width_a) = image_a.shape[:2]
    (height_b, width_b) = image_b.shape[:2]
    shortest_edge = min([height_a, width_a, height_b, width_b])
    if shortest_edge == height_a:
        image_b = resize(image_b, height = height_a, inter = inter)
    elif shortest_edge == height_b:
        image_a = resize(image_a, height = height_b, inter = inter)
    elif shortest_edge == width_a:
        image_b = resize(image_b, width = width_a, inter = inter)
    else:
        image_a = resize(image_a, width = width_b, inter = inter)
    return (image_a, image_b)

def relcrop(image_a, image_b, inter = cv2.INTER_AREA):
    '''
    Resize (if necessary) + crop images relative to one another
    '''
    if image_a.shape[0] != image_b.shape[0] and image_a.shape[1] != image_b.shape[1]:
        image_a, image_b = relresize(image_a, image_b, inter = inter)
    (height_a, width_a) = image_a.shape[:2]
    (height_b, width_b) = image_b.shape[:2]
    if height_a == height_b:
        if width_a > width_b:
            image_a = image_a[0:height_a, ((width_a - width_b) // 2):((width_a - width_b) // 2) + width_b]
        elif width_a < width_b:
            image_b = image_b[0:height_b, ((width_b - width_a) // 2):((width_b - width_a) // 2) + width_a]
    elif width_a == width_b:
        if height_a > height_b:
            image_a = image_a[((height_a - height_b) // 2):((height_a - height_b) // 2) + height_b, 0:width_a]
        elif height_a < height_b:
            image_b = image_b[((height_b - height_a) // 2):((height_b - height_a) // 2) + height_a, 0:width_b]
    return (image_a, image_b)

def preprocess(image, kernel: int = 5):
    '''
    Image preprocessing steps: resize, convert to grayscale, blur
    '''
    image = resize(image)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(image, (kernel, kernel), 0)
    return blurred

def simple(image, threshold: int, show = False):
    '''
    Simple threshold
    '''
    (T, thresh) = cv2.threshold(image, threshold, 255, cv2.THRESH_BINARY)
    if show is True:
        cv2.imshow("Simple threshold", thresh)
        cv2.waitKey(0)
    return thresh

def mean(image, blocksize: int = 11, C: int = 4, show = False):
    '''
    Mean threshold
    '''
    thresh = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, blocksize, C)
    if show is True:
        cv2.imshow("Mean threshold", thresh)
        cv2.waitKey(0)
    return thresh

def gaussian(image, blocksize: int = 15, C: int = 3, show = False):
    '''
    Gaussian threshold
    '''
    thresh = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, blocksize, C)
    if show is True:
        cv2.imshow("Gaussian threshold", thresh)
        cv2.waitKey(0)
    return thresh

def compareblurs(image):
    blurred = np.hstack([
    cv2.blur(image, (3, 3)),
    cv2.blur(image, (5, 5)),
    cv2.blur(image, (7, 7))
    ])
    cv2.imshow("Averaged", blurred)

    blurred = np.hstack([
    cv2.GaussianBlur(image, (3, 3), 0),
    cv2.GaussianBlur(image, (5, 5), 0),
    cv2.GaussianBlur(image, (7, 7), 0),
    ])
    cv2.imshow("Gaussian", blurred)

    blurred = np.hstack([
    cv2.medianBlur(image, 3),
    cv2.medianBlur(image, 5),
    cv2.medianBlur(image, 7)
    ])
    cv2.imshow("Median", blurred)

    blurred = np.hstack([
    cv2.bilateralFilter(image, 5, 21, 21),
    cv2.bilateralFilter(image, 7, 31, 31),
    cv2.bilateralFilter(image, 19, 41, 41),
    ])
    cv2.imshow("Bilateral", blurred)
    cv2.waitKey(0)


if mahotas_present is True:
    def otsu(image, show = False):
        '''
        Otsu threshold
        '''
        T = mahotas.thresholding.otsu(image)
        print("Otsu's thrshold: {}".format(T))
        thresh = image.copy()
        thresh[thresh > T] = 255
        thresh[thresh < 255] = 0
        if show is True:
            cv2.imshow("Otsu threshold", thresh)
            cv2.waitKey(0)
        return thresh

    def rc(image, show = False):
        '''
        Riddler-Calvard threshold
        '''
        T = mahotas.thresholding.rc(blurred)
        print("Riddler-Calvard threshold: {}".format(T))
        thresh = image.copy()
        thresh[thresh > T] = 255
        thresh[thresh < 255] = 0
        if show is True:
            cv2.imshow("Riddler-Calvard", thresh)
            cv2.waitKey(0)
        return thresh
