#!/usr/bin/env python3

import cv2
import numpy as np
import imutils

write_images = True

image = cv2.imread("images/sceau_texte.jpg")
cv2.imshow("Original", image)
cv2.waitKey(0)

# 3.1 Recadrer l'image et supprimer le texte
cropped = image[0:image.shape[0], 0:(2 * image.shape[1] // 3) - 7]
cv2.imshow("Cropped", cropped)
cv2.waitKey(0)

cropped_notext = cropped
cropped_notext[74:99] = (255, 255, 255)
cropped_notext[269:294] = (255, 255, 255)
cropped_notext[440:465] = (255, 255, 255)
cv2.imshow("No text", cropped_notext)
cv2.waitKey(0)
cv2.destroyAllWindows()

# 3.2 Compter les éléments
preprocessed = imutils.preprocess(cropped_notext)
edged = cv2.Canny(preprocessed, 30, 150) # image, seuil1, seuil2
cv2.imshow("Canny", edged)

edged2 = edged.copy()
(cnts, _) = cv2.findContours(edged2, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
print(f"Éléments trouvés dans l'image : {len(cnts)}")
cv2.waitKey(0)
cv2.destroyAllWindows()

# 3.3 Afficher les éléments un par un

for (i, c) in enumerate(cnts):
    (x, y, w, h) = cv2.boundingRect(c) # calcul du cadre (x début, y début, largeur, hauteur rectangle)

    print(f"Élément n° {i + 1}")
    object = cropped[y:y + h, x:x+ w] # une fois le contour trouvé, extraction à l'aide d'un tableau
    cv2.imshow("Object", object)
    cv2.waitKey(0)
cv2.destroyAllWindows()

# 3.4a Afficher seulement les sceaux 0345, 0346, 0352 et 0353 à l'aide d'un masque
center_seal_0345 = (269, 41)
center_seal_0346 = (370, 41)
center_seal_0352 = (276, 236)
center_seal_0353 = (373, 236)
centers = [center_seal_0345, center_seal_0346, center_seal_0352, center_seal_0353]

for center in centers:
    # masque rectangulaire
    mask = np.zeros(cropped_notext.shape[:2], dtype="uint8") #création d'une canevas de la même taille que notre image
    (cX, cY) = center
    cv2.rectangle(mask, (cX - 50, cY - 36), (cX + 50, cY + 36), 255, -1) # création d'un rectangle sur le masque de la taille souhaité. X et Y de début, X et Y de fin
    masked = cv2.bitwise_and(cropped_notext, cropped_notext, mask = mask) # tous les pixels de l'image seront "vrais", en appliquant le masque, seuls les pixels du masque seront actifs
    cv2.imshow("Mask applied to image", masked)
    cv2.waitKey(0)
cv2.destroyAllWindows()

# 3.4b Afficher seulement les sceaux 0345, 0346, 0352 et 0353 sans masque, à l'aide des index dans la question 3.2 : 3, 4, 5, 6, 13, 14, 15, 17

selected_seal_numbers = [2, 3, 4, 5, 12, 13, 14, 16]
selected_seals = []
max_seal_height = 0
max_seal_width = 0

for seal in selected_seal_numbers:
    c = cnts[seal]
    (_, _, w, h) = cv2.boundingRect(c)
    if w > max_seal_width:
        max_seal_width = w
    if h > max_seal_height:
        max_seal_height = h
    print(f"Élément n° {seal}: {w} x {h}")

for seal in selected_seal_numbers:
    c = cnts[seal]
    (x, y, w, h) = cv2.boundingRect(c)
    selected_seals.append(cropped[y:y + max_seal_height, x:x + max_seal_width])
# 3.5 enregistrer chacun des 8 sceaux de façon à obtenir 8 nouvelles images
    if write_images is True:
        object = cropped[y:y + h, x:x + w]
        try:
            cv2.imwrite(f"result/sceau_{seal}.png", object)
        except Exception:
            print("Erreur d'écriture du fichier")
selected_image = np.hstack(selected_seals)
cv2.imshow("Selected seals", selected_image)
cv2.waitKey(0)
