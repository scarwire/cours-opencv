# Problèmes d'installation :

54 bd raspail, salle 5 -- installation
mardi 16h-19h

# Processus de traitement d'une image

acquérir/stocker -> pre-processing (enhancement/restoration) -> segmentation -> représentation/description (reature descriptors/weights) -> recognition/interprétation

# couleur

- on interprète les longueurs d'ondes de ~400 à ~750 nm comme des couleurs. 
- lumière achromatique : lorsqu'elle contient toutes les longueurs

# teinte et saturation

- **teinte** : la longueur d'onde dominante. non additive 
- **saturation** : degré de dilution de la couleur dans la lumière
- **luminosité** : intensité de la lumière achromatique.

# couleur primaire

- selon la _loi de Grassman_ : rouge, vert, bleu
- ajoutées ensemble, R, V et B donnent de la lumière blanche
- l'oeil humain contient 3 sortes de cônes sensibles correspondant à 3 fréquences
- Trichromie -> récepteurs rétiniens

# synthèse additive

- rouge + bleu = magenta
- rouge + vert = jaune
- vert + bleu = cyan

# synthèse soustractive

- filtre jaune absorbe de la lumière bleue, laisse passer rouge + vert
- filtre magenta absorbe de la lumière verte, laisse passer rouge + bleu 
- filtre cyan absorbe de la lumière 

# absorption, diffusion, transmission

# image numérique

passage du 3d en 2d, composée d'unités élémentaires (pixels), cucun représentant une portion de l'image

# image -> élément discret

- on pose une grille pour rendre discrète et numériser
- plus il y a de pixels, plus la qualité est bonne

# image comme matrice

point d'origine (0,0) situé en haut et à gauche -> jusqu'à (w-1,h-1)

# caractéristique d'une image

- nombre de pixels
- étendue des teintes de chaque pixel
  - binaires : nb
  - niveaux de gris
  - couleurs

# images n+b 

matrice de pixels avec valeur noir (0) ou blanc (1)

# images en niveau de gris

- 2 niveaux : 1 bit
- 4 niveaux : 2 bits
- ...
- 256 niveaux : 8 bits
- 2^16 = 65536 niveaux : 16 bits

# images en couleurs

- système additif rgb
- chaque pixel à 24 bits (r,g,b)
- stockage image rgb
  - matrice 3 x n x mk
  - image indexée

# stockage image indexée - LUT

table de correspondance

# poids d'une image

nombre de pixels * nombre d'octets par pixel = nombre total d'octets dans l'image

# autres systèmes de représentation de la couleurs

- hsv = hue, saturation, valeur

# formats de fichiers

- header : info sur taille, colormap, compression, méthode
- data : pixel value (rgb), index values (correspondance tableau), ...
- formats communs
  - bitmap
  - compressée avec perte (jpeg)
  - compressée sans perte (tiff, png)
  - image vectorielle (eps, svg)

