#!/usr/bin/env python3

from __future__ import print_function # permet d'utiliser la fonction print() avec python 2.7 et 3.6
import numpy as np
import cv2 # librairie d'opencv avec les fonctions de traitement d'images

canvas = np.zeros((300, 300, 3), dtype="uint8")
(centreX, centreY) = (canvas.shape[1] // 2, canvas.shape[0] // 2) # largeur, hauteur
white = (255, 255, 255)

for r in range(0, 175, 25): # de 0 à 175 avec un pas de 25
    cv2.circle(canvas, (centreX, centreY), r, white)
    cv2.imshow("Canvas", canvas)
    cv2.waitKey(0)
