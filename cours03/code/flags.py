#!/usr/bin/env python3

from __future__ import print_function # permet d'utiliser la fonction print() avec python 2.7 et 3.6
import numpy as np
import cv2 # librairie d'opencv avec les fonctions de traitement d'images

# France

france_in = cv2.imread("../flags_in/france.png")
(b1, g1, r1) = france_in[1,1]
color1 = (int(b1), int(g1), int(r1))
(b2, g2, r2) = france_in[1,france_in.shape[1] // 2]
color2 = (int(b2), int(g2), int(r2))
(b3, g3, r3) = france_in[1,france_in.shape[1] - 1 ]
color3 = (int(b3), int(g3), int(r3))

flag_height = france_in.shape[0]
flag_width = france_in.shape[1]
canvas = np.zeros((flag_height, flag_width, 3), dtype="uint8")
cv2.rectangle(canvas,(0,0),(flag_width // 3, flag_height), color1, -1)
cv2.rectangle(canvas,(flag_width // 3, 0),(2 * flag_width // 3, flag_height), color2, -1)
cv2.rectangle(canvas,(2 * flag_width // 3, 0),(flag_width, flag_height), color3, -1)

cv2.imshow("France",canvas)
cv2.waitKey(0)
cv2.imwrite("../flags_out/france.png", canvas)

# Mali

mali_in = cv2.imread("../flags_in/mali.png")
(b1, g1, r1) = mali_in[1,1]
color1 = (int(b1), int(g1), int(r1))
(b2, g2, r2) = mali_in[1,mali_in.shape[1] // 2]
color2 = (int(b2), int(g2), int(r2))
(b3, g3, r3) = mali_in[1,mali_in.shape[1] - 1 ]
color3 = (int(b3), int(g3), int(r3))

flag_height = mali_in.shape[0]
flag_width = mali_in.shape[1]
canvas = np.zeros((flag_height, flag_width, 3), dtype="uint8")
cv2.rectangle(canvas,(0,0),(flag_width // 3, flag_height), color1, -1)
cv2.rectangle(canvas,(flag_width // 3, 0),(2 * flag_width // 3, flag_height), color2, -1)
cv2.rectangle(canvas,(2 * flag_width // 3, 0),(flag_width, flag_height), color3, -1)

cv2.imshow("Mali",canvas)
cv2.waitKey(0)
cv2.imwrite("../flags_out/mali.png", canvas)

# Luxembourg

lux_in = cv2.imread("../flags_in/lux.png")
(b1, g1, r1) = lux_in[1,1]
color1 = (int(b1), int(g1), int(r1))
(b2, g2, r2) = lux_in[lux_in.shape[0] // 2,1]
color2 = (int(b2), int(g2), int(r2))
(b3, g3, r3) = lux_in[lux_in.shape[0] - 1,1]
color3 = (int(b3), int(g3), int(r3))

flag_height = lux_in.shape[0]
flag_width = lux_in.shape[1]
canvas = np.zeros((flag_height, flag_width, 3), dtype="uint8")
cv2.rectangle(canvas,(0,0),(flag_width, flag_height // 3), color1, -1)
cv2.rectangle(canvas,(0,flag_height // 3),(flag_width, 2 * flag_height // 3), color2, -1)
cv2.rectangle(canvas,(0, 2 * flag_height // 3),(flag_width, flag_height), color3, -1)

cv2.imshow("Luxembourg",canvas)
cv2.waitKey(0)
cv2.imwrite("../flags_out/luxembourg.png", canvas)

# Bénin

benin_in = cv2.imread("../flags_in/benin.png")
(b1, g1, r1) = benin_in[1, 1]
color1 = (int(b1), int(g1), int(r1))
(b2, g2, r2) = benin_in[1, benin_in.shape[1] - 1]
color2 = (int(b2), int(g2), int(r2))
(b3, g3, r3) = benin_in[benin_in.shape[0] - 1, benin_in.shape[1] - 1]
color3 = (int(b3), int(g3), int(r3))

flag_height = benin_in.shape[0]
flag_width = benin_in.shape[1]
canvas = np.zeros((flag_height, flag_width, 3), dtype="uint8")
cv2.rectangle(canvas, (0, 0), (flag_width, flag_height // 2), color2, -1)
cv2.rectangle(canvas, (0, flag_height // 2), (flag_width, flag_height), color3, -1)
cv2.rectangle(canvas, (0, 0), (flag_width // 3, flag_height), color1, -1) # tracer rectangle vert en dernier pour qu'il aille au-dessus du rouge et du jaune

cv2.imshow("Bénin",canvas)
cv2.waitKey(0)
cv2.imwrite("../flags_out/benin.png", canvas)

# Japon

japon_in = cv2.imread("../flags_in/japon.png")
(b1, g1, r1) = japon_in[1, 1]
color1 = (int(b1), int(g1), int(r1))
(b2, g2, r2) = japon_in[japon_in.shape[0] // 2, japon_in.shape[1] // 2]
color2 = (int(b2), int(g2), int(r2))

flag_height = japon_in.shape[0]
flag_width = japon_in.shape[1]
canvas = np.zeros((flag_height, flag_width, 3), dtype="uint8")
cv2.rectangle(canvas, (0, 0), (flag_width, flag_height), color1, -1)
cv2.circle(canvas, (flag_width // 2, flag_height // 2), 3 * flag_height // 10, color2, -1) # (l'image, (x et y du centre du cercle), rayon du cercle, couleur du trait)

cv2.imshow("Japon",canvas)
cv2.waitKey(0)
cv2.imwrite("../flags_out/japon.png", canvas)

# Suisse

suisse_in = cv2.imread("../flags_in/suisse.png")
(b1, g1, r1) = suisse_in[1, 1]
color1 = (int(b1), int(g1), int(r1))
(b2, g2, r2) = suisse_in[suisse_in.shape[0] // 2, suisse_in.shape[1] // 2]
color2 = (int(b2), int(g2), int(r2))

flag_height = suisse_in.shape[0]
flag_width = suisse_in.shape[1]
canvas = np.zeros((flag_height, flag_width, 3), dtype="uint8")
cv2.rectangle(canvas, (0, 0), (flag_width, flag_height), color1, -1)
cv2.rectangle(canvas, (2 * flag_width // 5, flag_height // 5), (3 * flag_width // 5, 4 * flag_height // 5), color2, -1)
cv2.rectangle(canvas, (flag_width // 5, 2 * flag_height // 5), (4 * flag_width // 5, 3 * flag_height // 5), color2, -1)

cv2.imshow("Suisse",canvas)
cv2.waitKey(0)
cv2.imwrite("../flags_out/suisse.png", canvas)

# Royaume-Uni

# uk_in = cv2.imread("../flags_in/uk.png")
# (b1, g1, r1) = uk_in[1,uk_in.shape[1] // 3]
# color1 = (int(b1), int(g1), int(r1))
# (b2, g2, r2) = uk_in[1,10]
# color2 = (int(b2), int(g2), int(r2))
# (b3, g3, r3) = uk_in[10,1]
# color3 = (int(b3), int(g3), int(r3))
# 
# flag_height = uk_in.shape[0]
# flag_width = uk_in.shape[1]
# canvas = np.zeros((flag_height, flag_width, 3), dtype="uint8")
# cv2.rectangle(canvas,(0,0),(flag_width, flag_height), color1, -1)
# # cv2.rectangle(canvas,(0,flag_height // 3),(flag_width, 2 * flag_height // 3), color2, -1)
# # cv2.rectangle(canvas,(0, 2 * flag_height // 3),(flag_width, flag_height), color3, -1)
# cv2.line(canvas, (0, 0), (flag_width, flag_height), color2, flag_height // 6) # appel de l'image, x et y de départ, x et y de fin, couleur, largeur)
# cv2.line(canvas, (0, flag_height), (flag_width, 0), color2, flag_height // 6)
# cv2.imshow("Royaume-Uni",canvas)
# cv2.waitKey(0)
# cv2.imwrite("../flags_out/uk.png", canvas)