#!/usr/bin/env python3

from __future__ import print_function # permet d'utiliser la fonction print() avec python 2.7 et 3.6
import numpy as np
import cv2 # librairie d'opencv avec les fonctions de traitement d'images

canvas = np.zeros((300, 300, 3), dtype="uint8")

for i in range(0,25):
    radius = np.random.randint(5, high = 200) # taille du rayon
    color = np.random.randint(0, high = 256, size = (3,)).tolist() # couleur
    pt = np.random.randint(0, high = 300, size = (2,))
    cv2.circle(canvas, tuple(pt), radius, color, -1)
    cv2.imshow("Canvas", canvas)
    cv2.waitKey(0)
