#!/usr/bin/env python3

from __future__ import print_function # permet d'utiliser la fonction print() avec python 2.7 et 3.6
import numpy as np
import cv2 # librairie d'opencv avec les fonctions de traitement d'images

canvas = np.zeros((300,400,3), dtype="uint8")

red = (0,0,255)
green = (0,255,0)
blue = (255,0,0)

cv2.rectangle(canvas,(10,10),(60,60), green) # l'image, (x, y de départ), (x, y de fin), couleur
cv2.imshow("Canvas",canvas)
cv2.waitKey(0)

cv2.rectangle(canvas,(50, 200), (200, 225), red, 5) # dernier argument précise la taille du trait
cv2.imshow("Canvas",canvas)
cv2.waitKey(0)

cv2.rectangle(canvas,(200, 50), (225, 125), blue, -1) # si le dernier argument est négatif, rectangle est rempli
cv2.imshow("Canvas",canvas)
cv2.waitKey(0)