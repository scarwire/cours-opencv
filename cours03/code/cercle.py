#!/usr/bin/env python3

from __future__ import print_function # permet d'utiliser la fonction print() avec python 2.7 et 3.6
import numpy as np
import cv2 # librairie d'opencv avec les fonctions de traitement d'images

canvas = np.zeros((300,300,3), dtype="uint8")

red = (0,0,255)
green = (0,255,0)
blue = (255,0,0)
white = (255,255,255)

cv2.circle(canvas, (150,150), 45, white) # (l'image, (x et y du centre du cercle), rayon du cercle, couleur du trait)
cv2.imshow("Canvas",canvas)
cv2.waitKey(0)