# Rectangles

```
cv2.rectangle(canvas,(10,10),(60,60), green) # l'image, (x, y de départ), (x, y de fin), couleur
cv2.imshow("Canvas",canvas)
cv2.waitKey(0)
```

```
cv2.rectangle(canvas,(50, 200), (200, 225), green, 5) # dernier argument précise la taille du trait
cv2.imshow("Canvas",canvas)
cv2.waitKey(0)
```

```
cv2.rectangle(canvas,(200, 50), (225, 125), blue, -1) # si le dernier argument est négatif, rectangle est rempli
cv2.imshow("Canvas",canvas)
cv2.waitKey(0)
```

# Cercles

```
canvas = np.zeros(300, 300, 3), dtype="uint8"
white = (255,255,255)
cv2.circle(canvas, (150,150), 45, white) # (l'image, (x et y du centre du cercle), rayon du cercle, couleur du trait)
cv2.imshow("Canvas",canvas)
cv2.waitKey(0)
```

# Plusieurs Cercles

```
canvas = np.zeros((300, 300, 3), dtype="uint8")
(centreX, centreY) = (canvas.shape[1] // 2, canvas.shape[0] // 2) # largeur, hauteur
white = (255, 255, 255)

for r in range(0, 175, 25): # de 0 à 175 avec un pas de 25
    cv2.circle(canvas, (centreX, centreY), r, white)

cv2.imshow("Canvas", canvas)
cv2.waitKey(0)
```

# Dessin abstrait

```
for i in range(0,25):
    radius = np.random.randint(5, high = 200) # taille du rayon
```

# Exercice

Concevoir et enregistrer les images de drapeaux suivants :

- France
- Mali
- Luxembourg
- Bénin
- Japon
- Suisse