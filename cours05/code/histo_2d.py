#!/usr/bin/env python3

import numpy as np
import cv2
from matplotlib import pyplot as plt
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
image = cv2.imread(args["image"])

# redimensionnement de grandes images, max 1000 pixels côté long
if image.shape[1] > image.shape[0]:
    ratio = image.shape[1] / image.shape[0]
    if image.shape[1] > 1000:
        image = cv2.resize(image, (1000, int(1000 / ratio)), interpolation = cv2.INTER_AREA)
else:
    ratio = image.shape[0] / image.shape[1]
    if image.shape[0] > 1000:
        image = cv2.resize(image, (int(1000 / ratio), 1000), interpolation = cv2.INTER_AREA)

cv2.imshow("Original", image)
cv2.waitKey(0)

chans = cv2.split(image)

# bleu/vert
fig = plt.figure()
ax = fig.add_subplot(131)
hist = cv2.calcHist([chans[1], chans[0]], [0, 1], None, [32, 32], [0, 256, 0, 256])
p = ax.imshow(hist, interpolation = "nearest")
ax.set_title("2D color histogram for G and B")
plt.colorbar(p)
print("2D histogram shape: {}, with {} values".format(hist.shape, hist.flatten().shape[0]))

# vert/rouge
fig = plt.figure()
ax = fig.add_subplot(132)
hist = cv2.calcHist([chans[1], chans[2]], [0, 1], None, [32, 32], [0, 256, 0, 256])
p = ax.imshow(hist, interpolation = "nearest")
ax.set_title("2D color histogram for G and R")
plt.colorbar(p)
print("2D histogram shape: {}, with {} values".format(hist.shape, hist.flatten().shape[0]))

# bleu/rouge
fig = plt.figure()
ax = fig.add_subplot(133)
hist = cv2.calcHist([chans[0], chans[2]], [0, 1], None, [32, 32], [0, 256, 0, 256])
p = ax.imshow(hist, interpolation = "nearest")
ax.set_title("2D color histogram for B and R")
plt.colorbar(p)
print("2D histogram shape: {}, with {} values".format(hist.shape, hist.flatten().shape[0]))
plt.show()

# 2D histogramme, 3 canaux -- mais on ne peut pas l'afficher en 3D !

fig = plt.figure()
hist = cv2.calcHist([image], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
print("3D histogram shape: {}, with {} values".format(hist.shape, hist.flatten().shape[0]))