#!/usr/bin/env python3

import numpy as np
import cv2
from matplotlib import pyplot as plt
import argparse

# chargement de l'image depuis la ligne de commande
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
image = cv2.imread(args["image"])

# redimensionnement de grandes images, max 1000 pixels côté long
if image.shape[1] > image.shape[0]:
    ratio = image.shape[1] / image.shape[0]
    if image.shape[1] > 1000:
        image = cv2.resize(image, (1000, int(1000 / ratio)), interpolation = cv2.INTER_AREA)
else:
    ratio = image.shape[0] / image.shape[1]
    if image.shape[0] > 1000:
        image = cv2.resize(image, (int(1000 / ratio), 1000), interpolation = cv2.INTER_AREA)

cv2.imshow("Original", image)
cv2.waitKey(0)

chans = cv2.split(image)
colors = ("b", "g", "r")
plt.figure()
plt.title('"Flattened" color histogram')
plt.xlabel("Bins")
plt.ylabel("# of pixels")

# Loop over the image channels
for (chan, color) in zip(chans, colors):
    # create a histogram for the current channel and plot it
    hist = cv2.calcHist([chan], [0], None, [256], [0, 256])
    plt.plot(hist, color = color)
    plt.xlim([0, 256])
plt.show()