#!/usr/bin/env python3

import numpy as np
import cv2
from matplotlib import pyplot as plt
import argparse

# chargement de l'image depuis la ligne de commande
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
image = cv2.imread(args["image"])

# redimensionnement de grandes images, max 1000 pixels côté long
ratio = image.shape[1] / image.shape[0]
if image.shape[1] > image.shape[0] and image.shape[1] > 1000:
    image = cv2.resize(image, (1000, int(1000 / ratio)), interpolation = cv2.INTER_AREA)
elif image.shape[1] <= image.shape[0] and image.shape[0] > 1000:
    image = cv2.resize(image, (int(1000 * ratio), 1000), interpolation = cv2.INTER_AREA)

image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) # convertir l'image en échelle de gris
cv2.imshow("Grayscale image", image)
cv2.waitKey(0)

hist = cv2.calcHist(image, [0], None, [256], [0,256]) # histogramme en échelle de gris : [0]

plt.hist(image.ravel(),256,[0,256])
# plt.figure()
plt.title("Grayscale histogram") # ajout d'un titre
plt.xlabel("Bins") # légende horizontale
plt.ylabel("# of pixels") # légende verticale
# plt.plot(hist) # chargement de l'histogramme à afficher
plt.xlim([0, 256]) # échelle de l'axe horizontale
plt.show() # affichage de l'histogramme
