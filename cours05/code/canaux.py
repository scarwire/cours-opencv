#!/usr/bin/env python3

import numpy as np
import cv2
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
image = cv2.imread(args["image"])

if image.shape[1] in range(1000, 2000):
    image = cv2.resize(image, (int(image.shape[1] * 0.7), int(image.shape[0] * 0.7)), interpolation = cv2.INTER_AREA)
if image.shape[1] > 2000:
    image = cv2.resize(image, (int(image.shape[1] * 0.4), int(image.shape[0] * 0.4)), interpolation = cv2.INTER_AREA)

(B, G, R) = cv2.split(image)

# afficher chaque canal
cv2.imshow("Red", R)
cv2.waitKey(0)
cv2.imshow("Green", G)
cv2.waitKey(0)
cv2.imshow("Blue", B)
cv2.waitKey(0)

merged = cv2.merge([B, G, R]) # fusionner les canaux pour retrouver l'image d'origine
cv2.imshow("Merged", merged)
cv2.waitKey(0)

zeros = np.zeros(image.shape[:2], dtype="uint8")
cv2.imshow("Red", cv2.merge([zeros, zeros, R]))
cv2.waitKey(0)
cv2.imshow("Green", cv2.merge([zeros, G, zeros]))
cv2.waitKey(0)
cv2.imshow("Blue", cv2.merge([B, zeros, zeros]))
cv2.waitKey(0)
