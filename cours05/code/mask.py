#!/usr/bin/env python3

import numpy as np
import cv2
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
image = cv2.imread(args["image"])

if image.shape[1] in range(1000, 2000):
    image = cv2.resize(image, (int(image.shape[1] * 0.7), int(image.shape[0] * 0.7)), interpolation = cv2.INTER_AREA)
if image.shape[1] > 2000:
    image = cv2.resize(image, (int(image.shape[1] * 0.4), int(image.shape[0] * 0.4)), interpolation = cv2.INTER_AREA)

# masque rectangulaire
mask = np.zeros(image.shape[:2], dtype="uint8") #création d'une canevas de la même taille que notre image
(cX, cY) = (image.shape[1] // 2, image.shape[0] // 2) # définition du centre de l'image
cv2.rectangle(mask, (cX - 100, cY - 100), (cX + 100, cY + 100), 255, -1) # création d'un rectangle sur le masque de la taille souhaité. X et Y de début, X et Y de fin
cv2.imshow("Mask", mask)
cv2.waitKey(0)
masked = cv2.bitwise_and(image, image, mask = mask) # tous les pixels de l'image seront "vrais", en appliquant le masque, seuls les pixels du masque seront actifs
cv2.imshow("Mask applied to image", masked)
cv2.waitKey(0)

# masque circulaire
mask = np.zeros(image.shape[:2], dtype="uint8")
cv2.circle(mask, (cX, cY), 130, 255, -1)
masked = cv2.bitwise_and(image, image, mask = mask)
cv2.imshow("Mask", mask)
cv2.waitKey(0)
cv2.imshow("Mask applied to image", masked)
cv2.waitKey(0)