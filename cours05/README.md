# Opérateurs logiques

- AND, OR, XOR, NOT
- Fonctionnement binaire : 0 ou 1
- Un pixel est actif si l'opérateur est égal à 1, inactif s'il est égal à 0

## Exemple

```python
rectangle = np.zeros((300, 300), dtype="uint8")
cv2.rectangle(rectangle, (25, 25), (275,275), 255, -1)
cv2.imshow("Rectangle", rectangle)
cv2.waitKey(0)

circle = np.zeros((300, 300), dtype="uint8")
cv2.circle(circle, (150, 150), 150, 255, -1)
cv2.imshow("Circle", circle)
cv2.waitKey(0)
```

## `AND`, `OR`, `XOR`, `NOT`

```python
bitwiseAnd = cv2.bitwise_and(rectangle, circle)
cv2.imshow("AND", bitwiseAnd)
cv2.waitKey(0)

bitwiseOr = cv2.bitwise_or(rectangle, circle)
cv2.imshow("OR", bitwiseOr)
cv2.waitKey(0)

bitwiseXor = cv2.bitwise_xor(rectangle, circle)
cv2.imshow("XOR", bitwiseXor)
cv2.waitKey(0)

bitwiseNot = cv2.bitwise_not(circle)
cv2.imshow("OR", bitwiseNot)
cv2.waitKey(0)
```

- Avec `AND` :
  - noir + noir = noir
  - noir + blanc = noir
  - blanc + blanc = blanc
- Avec `OR` :
  - noir + noir = noir
  - noir + blanc = blanc
  - blanc + blanc = blanc
- Avec `XOR` :
  - noir + noir = noir
  - blanc + noir = blanc
  - blanc + blanc = noir
- Avec `NOT` :
  - blanc devient noir
  - noir devient blanc

Autrement dit :

- `AND` : est vrai uniquement si les deux pixels ont une valeur > 0
- `OR` : est vrai si au moins un pixel a une valeur > 0
- `XOR` : est vrai si uniquement un des deux pixels à une valeur > 0
- `NOT` inverse les valeurs d'un pixel

# Masque

- Permet de se focaliser sur juste une partie de l'image. (Intérêt : performance.) Exemple : pour reconnaître les visages, seules les zones où il y a des visages nous intéressent.
- On pourrait découper l'image avec "crop", avec le masque on conserve l'ensemble de l'image et on sélectionne la zone d'intérêt

## Masque rectangulaire

```python
mask = np.zeros(image.shape[:2], dtype="uint8") #création d'une canevas de la même taille que notre image
(cX, cY) = (image.shape[1] // 2, image.shape[0] // 2) # définition du centre de l'image
cv2.rectangle(mask, (cX - 75, cY - 75), (cX + 75, cY +75), 255, -1) # création d'un rectangle sur le masque de la taille souhaité. X et Y de début, X et Y de fin
cv2.imshow("Mask", mask)
cv2.waitKey(0)

masked = cv2.bitwise_and(image, image, mask = mask) # tous les pixels de l'image seront "vrais", en appliquant le masque, seuls les pixels du masque seront actifs
cv2.imshow("Mask applied to image", masked)
cv2.waitKey(0)
```

## Masque circulaire

```python
mask = np.zeros(image.shape[:2], dtype="uint8")
cv2.circle(mask, (cX, cY), 130, 255, -1)
masked = cv2.bitwise_and(image, image, mask = mask)
cv2.imshow("Mask", mask)
cv2.waitKey(0)
cv2.imshow("Mask applied to image", masked)
cv2.waitKey(0)
```

# Séparer et réunir les canaux

Les images en couleurs sont séparables en matrices rouge, verte, bleue : chacune contient des valeurs de 0 à 255

Pour afficher les canaux un par un (attention, représentation de chaque canal en dégradés de gris) :

```python
(B, G, R) = cv2.split(image)
cv2.imshow("Red", R)
cv2.waitKey(0)
cv2.imshow("Green", G)
cv2.waitKey(0)
cv2.imshow("Blue", B)
cv2.waitKey(0)
```

## Visualiser la couleur de nos canaux

Une autre méthode consiste à séparer les canaux mais reconsture l'image en mettant tous les canaux à 0 sauf celui qu'on veut visualiser :

```python
zeros = np.zeros(image.shape[:2], dtype="uint8")
cv2.imshow("Red", cv2.merge([zeros, zeros, R]))
cv2.waitKey(0)
cv2.imshow("Green", cv2.merge([zeros, G, zeros]))
cv2.waitKey(0)
cv2.imshow("Blue", cv2.merge([B, zeros, zeros]))
cv2.waitKey(0)
```

# Espaces de couleurs

- Hue, saturation, value (HSV)
  - Hue : teinte = couleur dominante
  - Saturation : coloration = pur ou délavé, intensité de la couleur
  - Value : luminosité = clarté ou luminosité de la couleur
- Une couleur vive a une luminosité moyenne ou forte, en une chromaticité élevée. Une couleur pâle a une luminosité moyenne ou forte et une chromaticité faible. Une couleur intense a une luminosité moyenne, voire faible, mais une chromaticité élevée.

- L\*a\*b\*
- La clarté L\* dérive de la luminance de la surface ; les deux paramètres a\* et b\* expriment l'écart de la couleur par rapport à celle d'une surface grise de même clarté.
- La clarté L\* prend des valeurs entre 0 (noir) et 100 (blanc de référence)
- Le paramètre a\* représente la valeur sur un axe vert --> rouge
- le paramètre b\* représente la valeur sur un axe bleu --> jaune
- a\* et b\* sont compris entre 127 à 128 pour représenter 256 valeurs

## Afficher selon des espaces de couleurs

```python
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow("Gray", gray)
cv2.waitKey(0)

hsv = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
cv2.imshow("HSV", hsv)
cv2.waitKey(0)

lab = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
cv2.imshow("L*a*b*", lab)
cv2.waitKey(0)
```

# Histogrammes

- Représente l'intensité des pixels d'une image
- Peut se visualiser sous forme de graphe (ou plot) qui donne une bonne impression de la distribution de l'intensité des valeurs des pixels
- On se situe dans un espace RGB où les pixels ont une valeur jusqu'à 255
- L'abscisse (axe horizontal) de l'histogramme représente le nombre de "bins". Si l'on utilise 256 bins on va représenter le nombre de chaque pixel. Si on utilise 2 bins on comptera le nombre de pixels compris entre \[0, 128\[ et \[128, 255\].
- L'ordonnée (axe vertical) représente le nombre de pixels pour chaque bin

## Implémenter un histogramme

- Utilisation de la fonction `cv2.calcHist`
  - `cv2.calcHist(image, channels, mask, histSize, ranges)`
- Image : l'image que l'on veut utiliser. Possibilité d'utiliser plusieurs images grâce à une liste : [img1, img2, img3...]
- Channels : une liste des canaux que l'on souhaite utiliser. Pour le noir et blanc : \[0\], 3 canaux : \[0, 1, 2\]
- Mask : si on utilise un masque l'histogramme ne prendra en compte que les pixels du masque. Si l'on ne veut pas de masque, on peut replacer cette valeur par none.
- histSize : le nombre de bins que l'on utilise pour chaque canal. Ici on utlisera 32 bins : `[32, 32, 32]`
- Range : pour RGB cette valeur est de \[0, 256\] pour chaque canal mais si on utilise un autre espace de couleur le range sera différent

## Histogramme 2D comparant deux couleurs :

```python
image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) # convertir l'image en échelle de gris
cv2.imshow("Original", image)
cv2.waitKey(0)

hist = cv2.calcHist(image, [0], None, [256], [0,256]) # histogramme en échelle de gris : [0]

plt.figure()
plt.title("Grayscale histogram") # ajout d'un titre
plt.xlabel("Bins") # légende horizontale
plt.ylabel("# of pixels") # légende verticale
plt.plot(hist) # chargement de l'histogramme à afficher
plt.xlim([0, 256]) # échelle de l'axe horizontale
```

- pour histogramme vert/rouge : `ax = fig.add_subplot(132)`
- pour histogramme bleu/rouge : `ax = fig.add_subplot(133)`