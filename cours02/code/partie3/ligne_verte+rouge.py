#!/usr/bin/env python3

from __future__ import print_function # permet d'utiliser la fonction print() avec python 2.7 et 3.6
import argparse # gérer les arguments du parsing en ligne de commande
import numpy as np
import cv2 # librairie d'opencv avec les fonctions de traitement d'images

canvas = np.zeros((300,300,3), dtype="uint8")

# chargement de l'image depuis la ligne de commande
'''
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
image = cv2.imread(args["image"])
'''

green = (0,255,0)
cv2.line(canvas,(0,0),(300,300),green) # appel de l'image, x et y de départ, x et y de fin, couleur)
cv2.imshow("Canvas",canvas)
cv2.waitKey(0)

red = (0,0,255)
cv2.line(canvas,(300,0),(0,300),red,3) # appel de l'image, x et y de départ, x et y de fin, couleur, largeur)
cv2.imshow("Canvas",canvas)
cv2.waitKey(0)