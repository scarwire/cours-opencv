#!/usr/bin/env python3

from __future__ import print_function # permet d'utiliser la fonction print() avec python 2.7 et 3.6
import argparse # gérer les arguments du parsing en ligne de commande
import cv2 # librairie d'opencv avec les fonctions de traitement d'images

# chargement de l'image depuis la ligne de commande
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
image = cv2.imread(args["image"])

# afficher les valeurs rgb du pixel (0,0)
(b, g, r) = image[0, 0]
print("Original pixel at (0,0): red {}, green {}, blue {}".format(r,g,b))

cv2.imshow("Original", image)
cv2.waitKey(0)

# attribuer rouge au pixel 0,0
image[0,0] = (0, 0, 255)
(b1,g1,r1) = image[0,0]
print("New pixel at (0,0): red {}, green {}, blue {}".format(r1,g1,b1))

corner = image[0:100,0:100] # départ de y:fin de y,départ de x:fin de x
cv2.imshow("Corner", corner)
cv2.waitKey(0)

image[0:100,0:100] = (0,255,0)
cv2.imshow("Updated", image)
cv2.waitKey(0)