#!/usr/bin/env python3

from __future__ import print_function # permet d'utiliser la fonction print() avec python 2.7 et 3.6
import argparse # gérer les arguments du parsing en ligne de commande
import cv2 # librairie d'opencv avec les fonctions de traitement d'images

# chargement de l'image depuis la ligne de commande
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
image = cv2.imread(args["image"])

cv2.imshow("Original",image)
cv2.waitKey(0)

# découper
height = image.shape[0]
corner = image[height - 200:height,0:120] # départ de y:fin de y,départ de x:fin de x
cv2.imshow("Corner", corner)
cv2.waitKey(0)

# colorier
corner[0:200,0:120] = (0,0,255)
cv2.imshow("Original with colored corner", image)
cv2.waitKey(0)

# sauvegarder
cv2.imwrite("bloc_rouge.jpg", corner)