#!/usr/bin/env python3

from __future__ import print_function # permet d'utiliser la fonction print() avec python 2.7 et 3.6
import argparse # gérer les arguments du parsing en ligne de commande
import cv2 # librairie d'opencv avec les fonctions de traitement d'images

# chargement de l'image depuis la ligne de commande
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
image = cv2.imread(args["image"])

cv2.imshow("Original",image)
cv2.waitKey(0)

# découper
height = image.shape[0]
width = image.shape[1]
center = image[height // 2 - 300:height // 2 + 300, width // 2 - 200:width // 2 + 200] # départ de y:fin de y,départ de x:fin de x
cv2.imshow("Center", center)
cv2.waitKey(0)

# colorier
center[0:center.shape[0],0:center.shape[1]] = (0,0,255)
cv2.imshow("Original with colored center", image)
cv2.waitKey(0)

# sauvegarder
cv2.imwrite("bloc_rouge.jpg", center)