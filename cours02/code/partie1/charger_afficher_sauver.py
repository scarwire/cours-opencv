#!/usr/bin/env python3

from __future__ import print_function # permet d'utiliser la fonction print() avec python 2.7 et 3.6
import argparse # gérer les arguments du parsing en ligne de commande
import cv2 # librairie d'opencv avec les fonctions de traitement d'images

# chargement de l'image depuis la ligne de commande
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
#image = cv2.imread("../images/doa.jpg")
image = cv2.imread(args["image"])

# afficher largeur, hauteur, nombre de canaux
print("largeur : {} pixels".format(image.shape[1]))
print("hauteur : {} pixels".format(image.shape[0]))
print("canaux : {}".format(image.shape[2]))

# afficher l'image à l'écran
cv2.imshow("Image",image)
cv2.waitKey(0) # durée en ms. avec 0 l'image disparaît lorsque l'utilisateur clique sur une touche du clavier. commande à placer après image = cv2.imread() et cv2.imshow()

# sauvegarder une copie
cv2.imwrite("image_copie.jpg",image)