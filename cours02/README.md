# Séminaire OpenCV n° 2, 6 février 2019

## Installation d'OpenCV pour python

Dans un premier temps, il s'agit d'installer toutes les librairies nécessaires pour que python puisse accéder aux fonctions d'OpenCV. Ces librairies peuvent être installées avec le gestionnaire de pacquets `pip`.

- OpenCV : `sudo pip install opencv-contrib-python`
- Matplotlib : `sudo pip install matplotlib`
- scikit-learn : `sudo pip install scikit-learn`
- scikit-image : `sudo pip install scikit-image`

## Partie 1 : Charger, afficher, sauvegarder des images

Nous avons créé un [premier script](code/partie1/charger_afficher.sauver.py) qui charge, affiche et sauvegarde des images.

Le script commence par trois lignes qui chargent les librairies et fonctions nécessaires :

```
from __future__ import print_function
import argparse
import cv2
```

Initialement, nous avons chargé une image directement dans le script en l'attribuant au variable `image` : `image = cv2.imread("../images/doa.jpg")`. Ensuite, pour pouvoir charger cette image directement depuis la ligne de commande, nous avons employé un bout de code avec les fonctions de la librairie `argparse`.

```
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
```

`cv2.imread(../images/chat.jpg)` est remplacé par `cv2.imread(args["image"])`.

### Affichage de l'image

- Par défaut, la fonction `cv2.imread()` affiche l'image et la fait disparaître immédiatement. Il faut donc rajouter une commande après : `cv2.waitKey(0)`. Le `(0)` précise que l'ordinateur va attendre qu'on appuie sur un bouton pour fermer l'image. Si on met un autre chiffre _n_ à sa place, l'image sera affichée pendant _n_ millisecondes.

### Accéder aux données de l'image

La fonction `shape` permet d'accéder aux dimensions de l'image : sa hauteur, sa largeur et le nombre de canaux :

- `print("largeur : {} pixels".format(image.shape[1]))` affiche la largeur
- `print("hauteur : {} pixels".format(image.shape[0]))` affiche la hauteur
- `print("canaux : {}".format(image.shape[2]))` affiche le nombre de canaux (1 pour noir et blanc, 2 pour dégradés de gris, 3 pour couleurs)

### Enregistrer une copie de l'image

- Pour enregistrer une copie : `cv2.imwrite("copie_image.jpg",image)`
- Pour convertir le format, il suffit de changer l'extension : `cv2.imwrite("copie_image.png",image)`

## Partie 2 : colorier, découper des régions

### L'image en tant que matrice de pixels

- Pixel : unité élémentaire d'une image
- une image avec une résolution de 400x200 signifie qu'elle est composée de 400 lignes, 200 colonnes
- pixel : noir ou blanc, échelle de gris ou couleur
- couleur 8 bits : 256 nuances de R, G et B dans une intervalle de \[0,255\]
  - blanc : (255,255,255)
  - noir : (0,0,0)
- Source d'erreur fréquente dans OpenCV : sur la matrice des pixels, l'origine (0,0) se situe **en haut et à gauche** : (largeur, hauteur)

### Connaître et changer la couleur d'un pixel 

Les scripts de la partie 2 permettent de modifier des pixels ou des régions d'une image. Le [premier script](code/partie2/access_parameter.py) modifie le premier pixel en haut et à gauche d'une image.

- OpenCV enregistre les canaux RGB **dans le sens inverse** : BGR.
  - `(b, g, r) = image[0, 0]`
  - `print("Pixel at (0,0): red {}, green {}, blue {}".format(r,g,b))`
- Pour attribuer une couleur à un pixel :
  ```
  image[0,0] = (0, 0, 255) # attribuer rouge au pixel 0,0
  (b1,g1,r1) = image[0,0]
  print("Pixel at (0,0): red {}, green {}, blue {}".format(r1,g1,b1))
  ```

### Découper un morceau d'une image

Le [deuxième script](code/partie2/bloc_rouge_2.py) dans cette partie va découper un coin de l'image et le colorier.

- `corner = image[0:100,0:100]` sélectionne une région : \[départ de y:fin de y,départ de x:fin de x\]
- On peut aussi sélectionner utilisant des divisions : par exemple `img.shape[0]//2` spécifie une coordonnée située à la moitié de la hauteur de l'image. Avec cette technique, attention à utiliser une division qui donne des toujours des nombres entiers : `//` au lieu du simple `/`. Il faut toujours un nombre entier de pixels, OpenCV ne sait pas arrondir !

Le [troisième script](code/partie2/bloc_rouge_3.py) colorie une région au milieu de l'image, puis découpe cette région et la sauvegarde dans un nouveau fichier.

## Partie 3 : dessiner

Le [script de la partie finale](code/partie3/ligne_verte+rouge.py) du cours permet de créer une nouvelle image sans aucune donnée et de tracer des lignes dessus :

- Charger des packages au début : `import numpy as np` (np devient l'alias de numpy), `import cv2`
- Création d'une image : `canvas = np.zeros((300,300,3), dtype="uint8")`
  - Il s'agit d'un tableau de 300 colonnes * 300 lignes, allocation de 3 canaux pour la couleur, utilisation de 8 bits d'entier (256 couleurs). `np.zeros` : toutes les valeurs du tableau sont à 0.

- Pour tracer une ligne verte :
  ```
  green = (0,255,0)
  cv2.line(canvas,(0,0),(300,300),green) # appel de l'image, x et y de départ, x et y de fin, couleur)
  cv2.imshow("Canvas",canvas)
  cv2.waitKey(0)
  ```

- Tracer une ligne rouge de trois pixels de largeur :
  ```
  red = (0,0,255)
  cv2.line(canvas,(300,0),(0,300),red,3) # appel de l'image, x et y de départ, x et y de fin, couleur, largeur)
  cv2.imshow("Canvas",canvas)
  cv2.waitKey(0)
  ```