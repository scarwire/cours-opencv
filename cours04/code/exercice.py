#!/usr/bin/env python3

import cv2
import numpy as np
import imutils
import argparse

# chargement de l'image depuis la ligne de commande
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
image = cv2.imread(args["image"])
cv2.imshow("Original", image)
cv2.waitKey(0)

shifted = imutils.translate(image, -30, -40)
cv2.imshow("Shifted", shifted)
cv2.waitKey(0)

rotated = imutils.rotate(shifted, -45, (0, shifted.shape[0]), 0.8)
cv2.imshow("Rotated and diminished", rotated)
cv2.waitKey(0)

flipped = cv2.flip(rotated, 1)
cv2.imshow("Flipped horizontally", flipped)
cv2.waitKey(0)

resized = imutils.resize(flipped, int(flipped.shape[1] * 1.4), int(flipped.shape[0] * 1.4))
cv2.imshow("Resized", resized)
cv2.waitKey(0)
cv2.imwrite("../images/result.png", resized)