#!/usr/bin/env python3

import cv2
import numpy as np
import imutils
import argparse

# chargement de l'image depuis la ligne de commande
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
image = cv2.imread(args["image"])
cv2.imshow("Original", image)
cv2.waitKey(0)

# rotation
(h, w) = image.shape[:2]
center = (w//2, h//2)

M = cv2.getRotationMatrix2D(center, 45, 1.0) # (centre, angle, échelle) -> sens horaire
rotated = cv2.warpAffine(image, M, (w, h))
cv2.imshow("Rotated by 45 degrees", rotated)
cv2.waitKey(0)