#!/usr/bin/env python3

# Chargement de l'image
import numpy as np
import cv2
image = cv2.imread("../images/doa.jpg")
cv2.imshow("Original", image)
cv2. waitKey(0)

# Translation
M = np.float32([[1, 0, 80], [0, 1, 50]]) # [x boolean, y boolean, nb de pixels]
shifted = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))
cv2.imshow("Shifted down and right", shifted)
cv2.waitKey(0)

M1 = np.float32([[1, 0, -80], [0, 1, -120]]) # [x boolean, y boolean, nb de pixels]
shifted = cv2.warpAffine(image, M1, (image.shape[1], image.shape[0]))
cv2.imshow("Shifted up and left", shifted)
cv2.waitKey(0)

