#!/usr/bin/env python3

import cv2
import numpy as np
import imutils
import argparse

# chargement de l'image depuis la ligne de commande
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
image = cv2.imread(args["image"])
cv2.imshow("Original", image)
cv2.waitKey(0)

# recadrage
cropped = image[0:image.shape[0], 0:image.shape[1] * 2 // 3] # y de début:y de fin, x début:x fin
cv2.imshow("Cropped", cropped)
cv2.waitKey(0)