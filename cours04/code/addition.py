#!/usr/bin/env python3

import cv2
import numpy as np
import argparse

# chargement de l'image depuis la ligne de commande
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
image = cv2.imread(args["image"])
cv2.imshow("Original", image)
cv2.waitKey(0)

# addition
print("opencv max of 255: {}".format(cv2.add(np.uint8([200]), np.uint8([100]))))
print("openv min of 0: {}".format(cv2.subtract(np.uint8([50]), np.uint8([100]))))
print("numpy wrap around: {}".format(np.uint8([200]) + np.uint8([100])))
print("numpy wrap around: {}".format(np.uint8([50]) - np.uint8([100])))