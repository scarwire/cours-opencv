#!/usr/bin/env python3

import cv2
import numpy as np
import imutils
import argparse

# chargement de l'image depuis la ligne de commande
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
image = cv2.imread(args["image"])
cv2.imshow("Original", image)
cv2.waitKey(0)

# redimensionnement
r = 1100.0 / image.shape[1] # définition du ratio
dim = (1100, int(image.shape[0] * r)) # taille nouvelle image

resized = cv2.resize(image, dim, interpolation = cv2.INTER_AREA) #image, nouvelle dimension, méthode
cv2.imshow("Resized (width)", resized)
# autres options : cv2.INTER_AREA, cv2.INTER_LINEAR, cv2.INTER_CUBIC, cv2.INTER_NEAREST
cv2.waitKey(0)

resized = imutils.resize(image, width = 1300)
cv2.imshow("Resized via function", resized)
cv2.waitKey(0)