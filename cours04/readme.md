# Transformation de l'image

## Translation

Déplacemdent d'une image le long des axes x et y.


```python
# Chargement de l'image
import numpy as np
import cv2
image = cv2.imread("../images/chat.png")
cv2.imshow("Original", image)
cv2. waitKey(0)

# Trlanslation
M = np.float32([[1, 0, 25], [0, 1, 50]]) # [y boolean, x boolean, nb de pixels]
shifted = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))
cv2.imshow("Shifted down and right", shifted)
cv2.waitKey(0)
```

## Création imutils.py

Créer un fichier `imutils.py` dans le même dossier que votre script Translation

```python
import numpy as np
import cv2
def translate(image, x, y):
    M = np.float32([[1, 0, x], [0, 1, y]])
    shifted = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))
    return shifted
```

## Utilisation de imutils

```python
import imutils

shifted = imutils.translate(image, 0, 100)
cv2.imshow("Shifted down", shifted)
cv2.waitKey(0)
```


# Exercice

- Réaliser une translation de 40 pixels en haut et 30 à gauche
- Faire une rotation à partir du coin inférieur gauche de l'image de -45°
- Redimensionner l'image en diminuant ss dimensions de 0.8
- Faire un retournement horizontal
- Finir par recadrer comme on veut


# Arithmétique sur des images

- Ex : addition, soustraction
- Que se passe-t-il si on a un pixel en rgb avec intensité de 250 et on ajoute 10
- NumPy: modulo addition (wrap around : 250 + 10 = 4)
- OpenCV: bloque à 255