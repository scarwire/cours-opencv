#!/usr/bin/env python3

import cv2
import numpy as np
import argparse
import imutils

# chargement de l'image depuis la ligne de commande
ap = argparse.ArgumentParser()
ap.add_argument("imageA", help="Path to image A")
ap.add_argument("imageB", help="Path to image B")
args = vars(ap.parse_args())
imageA = cv2.imread(args["imageA"])
imageB = cv2.imread(args["imageB"])

imageA, imageB = imutils.relcrop(imageA, imageB)
if min([imageA.shape[0], imageA.shape[1], imageB.shape[0], imageB.shape[1]]) > 1000:
    imageA = imutils.max1000px(imageA)
    imageB = imutils.max1000px(imageB)

print(f"Image A height {imageA.shape[0]} x width {imageA.shape[1]}")
print(f"Image B height {imageB.shape[0]} x width {imageB.shape[1]}")

subtracted = cv2.subtract(imageA, imageB)
cv2.imshow("Subtracted", subtracted)
cv2.waitKey(0)
