#!/usr/bin/env python3

import cv2
import numpy as np
import argparse
import imutils

# chargement de l'image depuis la ligne de commande
ap = argparse.ArgumentParser()
ap.add_argument("imageA", help="Path to image A")
ap.add_argument("imageB", help="Path to image B")
args = vars(ap.parse_args())
imageA = cv2.imread(args["imageA"])
imageB = cv2.imread(args["imageB"])
imageA = imutils.resize(imageA, width=750)
imageB = imutils.resize(imageB, width=250)

print(f"Image A height {imageA.shape[0]} x width {imageA.shape[1]}")
print(f"Image B height {imageB.shape[0]} x width {imageB.shape[1]}")
(cX, cY) = (imageA.shape[1] // 2, imageA.shape[0] // 2) # définition du centre de l'image
imageA[cY - imageB.shape[0] // 2:1 + cY + imageB.shape[0] // 2, cX - imageB.shape[1] // 2:cX + imageB.shape[1] // 2] = imageB

cv2.imshow("superimposed", imageA)
cv2.waitKey(0)
