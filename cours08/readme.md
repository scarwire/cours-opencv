# Apprentissage automatique

- Machine learning/deep learning
- Classification d'une image
- Application à des grands datasets pour identifier l'âge et le genre d'une personne, type et modèle de véhicule, reconnaissance d'expression faciale, écriture à la main

## Librairies :

- Principalement deux librairies de deep learning :
  - Keras
  - Mxnet
- Traitement d'image :
  - OpenCV
  - scikit-image, scikit-learn

## Deep learning

- Notion d'abstraction

> Deep learning methods are representation-learning methods with multiple levels of representation, obtained by composing simple but nonlinear modules that each transform the representation at one level (starting with the raw input) into a representation at a higher, slightly more abstract level. \[...] The key aspect of deep learning is that these layers are not designed by human engineers: they are learned from data using a general-purpose learning procedure"

Yann LeCun, Yoshua Bengio, Geoffrey Hinton, _Nature_, 2015

## IA, machine learning, deep learning

- (intelligence artificielle(machine learning(Deep learning)))
- Deep learning est un sous-domaine du machine learning qui est un sous-domaine de l'intelligence artificielle
- L'objectif central de l'IA est de fournir un ensemble d'algorithmes et de techniques qui peuvent être utilisés pour résoudre des problèmes que les humains exécutent intuitivement et presque automatiquement, mais qui sont très complexes pour les ordinateurs. Par exemple comprendre le contenu d'une image.
- Le machine learning tend à apprendre grâce à des données
- Deep learning fait référence à des algorithmes d'apprentissage basé sur l'abstraction.

## Historique

- 1943 : premier modèle de réseau neuronal (McCulloch et Pitts). Ce réseau était un classificateur inaire, capable de reconnaître deux catégoriex différentes en fonction de certaines donnnées d'entrée. Le problème était que les poids utilisés pour déterminer l'étiquette de la classe pour une entrée donnée devaient être réglage manuel par un humain -- ce type de modèle ne s'adapte clairement pas bien si l'opérateur humain doit intervenir.
- Années 1950 : Perceptron (Rosenblatt) - peut automatiquement apprendre les poids requis pour classifier des données (aucune intervention humaine nécessaire)
- inputs > weights > weighted sum > step function (fonction de seuil)

## Inspiration du cerveau

- Fonctionnalité inspirée du cerveau humain mais pas une tentative de le reproduire
- Le cerveau humain a environ 1011 neurones liés à 1015 synapses
- Les signaux électrochimiques sont transmis entre les neurones liés, le signal est transmis si le potentiel d'activation dépasse un seuil
- Apprentissage : les synapses et leur propriétés de transmission changent avec le temps

## Problèmes non linéaires

- 1969 : Minsky et Papert ont montré qu'un perceptron doné d'un capteur linéaire...

## Convolutional neural network

- 1988, Lecun : appliqué à la reconnaissance de caractères manuscrits, qui apprend automatiquement à discriminer les motifs (appelés "filtres") des images en les empilant séquentiellement les couches les unes sur les autres. Les filtres des niveaux inférieurs du réseau représentent les bords et les coins, tandis que les couches de niveau supérieur utilisent les bords et les coins pour apprendre les concepts plus abstraits utiles pour faire la distinction entre les classes d'images.
- Dans de nombreuses applications, les CNN sont maintenant considérés comme le classificateur d'images le plus puissant.

![CNN](https://upload.wikimedia.org/wikipedia/commons/6/63/Typical_cnn.png)

- Back propagation : réadaptation des poids selon feedback humain qui corrige le résultat

## 3 types d'algorithmes machine learning

- Supervisé, semi supervisé, non supervisé
- supervisé : on identifie d'abord et les entrées et les sorties. Par ex. : on donne une image d'un chat, on dit à l'ordinateur qu'on veut l'identifier en tant que tel.
- Non supervisé : beaucoup plus compliqué -- on n'indique pas les classes de sortie souhaitées. L'algorithme doit apprendre à trouver des motifs ou caractéristiques communes.
- Semi supervisé : images étiquetées ET non étiquetées comme données d'entrée, classes de sortie déjà identifiées.

## Apprentissage hiérarchique

- Hiérarchise  les concepts. Les concepts des couches de niveau inférieur du réseau codent quelques représentations de base du problème, alors que les couches de niveau supérieur utilisent ces couches de base pour former plus des concepts abstraits.
- Dans un premier temps, seules les régions marinales sont détectées...

## Base de la classification d'image

- À défaut de faire comprendre le sens d'une image à un algorithme nous devons appliquer la classification d'image qui extrait le sens d'une image par l'assignation de labels sur les contenus d'une image, interpréter son contenu ou retourner une phrase

## Classification d'images

- La tâche est d'attribuer une étiquette à une image à partir d'un ensemble de catégories.
- Concrètement, cela signifie que notre tâche est d'analyser une image d’entrée et de retourner une étiquette qui catégorise l'image.
- L'étiquette provient toujours d'un ensemble prédéfini 

## Output souhaités

- Notre système de classification pourait attribuer plusieurs étiquettes à l'image par le biais de probabilités : chien 95%, chat 4%, panda 1%
- Plus formellement, étant donné notre image d'entrée de pixels W\*H avec trois canaux, notre objectif est de prendre l'image W\*H*3=N...

## Terminologie

- Avec le machine learning et le deep learning, nous disposons d'un ensemble de données où nous essayons d'extraire de la connaissance de chaque élément de l'ensemble de données : un point de données (data point).
- Un ensemble de données (dataset) est donc un ensemble de points de données (data points).
- Notre but est d'appliquer des algorithmes de DL pour découvrir les principes sous-jacents dans l'ensemble des données, ce qui nous permet de classer correctement les points de données que notre algorithme ne possède pas que vous n'avez pas encore rencontré.

## Gap sémantique

- La différence entre la façon dont un être humain perçoit le contenu d'une image par rapport à la façon dot une image peut être représentée de manière à ce qu'on ordinateur puisse la comprendre.

## Un exemple : 

- Spatial : ciel en haut, sable/océan en bas.
- Couleur : ciel bleu foncé, eau de l'océan bleu plus clair que le ciel, sable

## Défis pour un ordinateur

- Taille (scale variation)
- couleur
- identifier ensemble/partie (occlusion variation)
- orientation/point de vue
- déformation d'objets (différences intra-classe)
- luminosité, contraste, netteté
- fond (background clutter)

## Overfitting

- Attention ! Si vos données sont trop proches les unes des autres, présence d'un biais. Exemple : toujours utiliser des images de chats roux, de la même espèce avec un fond bleu.

---

# Exercice

1. Addition de deux images. (9 lignes)
2. Insérer une image dans une autre (utiliser la fonction `resize` déja définie en cours 4).
3. Afficher la comparaison de deux images. Par exemple deux images "proches" d'une séquence vidéo (9).
4. Afficher une image qui est la superposition de deux images. (11l)
- Bonus : créer une fonction pour chacune des questions précédentes

---

# Deep learning

(diapos Michele Alberti)

## Why is it mainstream only now?

- Larger labeled datasets
- More computational power

## Échelle

- Large dataset: several million samples
- Human brain: 1000 trillion synapses
- ResNet-152: 60 million synapses
- Human brains have ~15 million times the synapses (parameters) of artificial neural networks!

## How to train your model (simplified)

inputs -> neural network -> outputs + solution > corrections (back propagation to neural network)

## Kernel positioning

- Padding
- Stride size

$$ o = [\frac{i + 2p - k}{s}] + 1 $$

o = output size  
i = input size  
p = padding  
k = kernel size (on one side)  
s = stride

## Many names, same thing

- number of output channels
- number of output planes
- number of neurons
- number of features
- number of kernels/filters

exemple :

```python
nn.Conv2d(input_channels=3, output_channels=12, \
          kernel_size=5, stride=3, padding=1)
```

## "Traditional" computer vision

- Classification
- Classification + localization
- object detection
- image segmentation

## Deep learning

on peut ajouter :

- colorization
- Sketch to 3D object
- content-based image retrieval
