#!/usr/bin/env python3

import cv2
import numpy as np
import argparse
import imutils

# chargement de l'image depuis la ligne de commande
ap = argparse.ArgumentParser()
ap.add_argument("imageA", nargs="?", default="images/betty2.png", help="Path to image A")
ap.add_argument("imageB", nargs="?", default="images/betty1.png", help="Path to image B")
args = vars(ap.parse_args())
image_a = cv2.imread(args["imageA"])
image_b = cv2.imread(args["imageB"])

image_a, image_b = imutils.relcrop(image_a, image_b)
if min([image_a.shape[0], image_a.shape[1], image_b.shape[0], image_b.shape[1]]) > 1000:
    image_a = imutils.max1000px(image_a)
    image_b = imutils.max1000px(image_b)

cv2.imshow("Image A", image_a)
cv2.imshow("Image B", image_b)
print(f"Image A height {image_a.shape[0]} x width {image_a.shape[1]}")
print(f"Image B height {image_b.shape[0]} x width {image_b.shape[1]}")

# filtre de Canny
edged_a = cv2.Canny(imutils.preprocess(image_a), 15, 65) # image, seuil1, seuil2

(cnts, _) = cv2.findContours(edged_a, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

contours = cv2.drawContours(image_b, cnts, -1, (127, 255, 0), 2)
cv2.imshow("Comparison", contours)
cv2.waitKey(0)
